var map = L.map('map').setView([-33.87, 179], 5);
var layer = L.esri.basemapLayer('Topographic').addTo(map);
var layerLabels;

function setBasemap(basemap) {
if (layer) {
  map.removeLayer(layer);
}

layer = L.esri.basemapLayer(basemap);

map.addLayer(layer);

if (layerLabels) {
  map.removeLayer(layerLabels);
}

if (basemap === 'ShadedRelief'
 || basemap === 'Oceans'
 || basemap === 'Gray'
 || basemap === 'DarkGray'
 || basemap === 'Imagery'
 || basemap === 'Terrain'
) {
  layerLabels = L.esri.basemapLayer(basemap + 'Labels');
  map.addLayer(layerLabels);
}
}

function changeBasemap(basemaps){
var basemap = basemaps.value;
setBasemap(basemap);
}


// FeatureGroup is to store editable layers
var drawnItems = new L.FeatureGroup();
map.addLayer(drawnItems);


var drawControl = new L.Control.Draw({
    draw: {
        polygon: {
            shapeOptions: {
                color: 'purple'
                },
            allowIntersection: false,
            drawError: {
                color: 'orange',
                timeout: 1000
                },
        },
        marker: false,
        polyline: false,
        circle: false
    },
    edit: {
        featureGroup: drawnItems
    }
});
map.addControl(drawControl);

function getColor(d) {
    return d > 700 ? '#800026' :
           d > 600 ? '#bd0026' :
           d > 500 ? '#e31a1c' :
           d > 400 ? '#fc4e2a' :
           d > 300 ? '#fd8d3c' :
           d > 200 ? '#feb24c' :
           d > 100 ? '#fed976' :
                     '#ffffb2';
}

var geojsonMarkerOptions = {
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};


var events = {};

// load GeoJSON from an external file
$.getJSON("earthquakes_selection.geojson",function(data){
    L.geoJson(data,{
        style: function(feature) {
            var size = (Math.pow(1.5, feature.properties.mag)/2);
            return {fillColor: getColor(feature.geometry.coordinates[2]), radius: size};
        },
        pointToLayer: function(feature, latlng){
            marker = L.circleMarker(latlng, geojsonMarkerOptions);
            marker.bindPopup(feature.properties.title + '<br/>' + 'Depth (km): ' + feature.geometry.coordinates[2]);
            events[feature.properties.code] = {"marker": marker};
            return marker;
        }
    }).addTo(map);
});

// From http://stackoverflow.com/questions/31790344/determine-if-a-point-reside-inside-a-leaflet-polygon
function isMarkerInsidePolygon(marker, poly) {
    var polyPoints = poly.getLatLngs()[0];
    var x = marker.getLatLng().lat, y = marker.getLatLng().lng;

    var inside = false;
    for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
        var xi = polyPoints[i].lat, yi = polyPoints[i].lng;
        var xj = polyPoints[j].lat, yj = polyPoints[j].lng;

        var intersect = ((yi > y) != (yj > y))
            && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }

    return inside;
}

function setMarkerInactive(value) {
    value.marker.setStyle({opacity: 0.1, fillOpacity: 0.1});
}

function setAllActive() {
    _.forEach(events, function(value, key) {
        value.marker.setStyle({opacity: 1, fillOpacity: 0.8});
    });
}

function setSomeInactive(poly_id) {
    var selected_events = {};
    _.forEach(events, function(value, key) {
        var query = isMarkerInsidePolygon(value.marker, drawnItems._layers[poly_id]);
        if (query) {
            //write selected quakes json
            inside_event = value.marker
            selected_events[value.marker.feature.id] = {"time": value.marker.feature.properties.time};
        } else {
            setMarkerInactive(value)
        }
    });
    string_earthquakes = JSON.stringify(selected_events)
    window.alert(string_earthquakes)
}

map.on('draw:drawstart', function (e) {
    // Remove all other polygons
    drawnItems.eachLayer(function (old_layer) {
        drawnItems.removeLayer(old_layer);
    });
    setAllActive();
});

map.on('draw:created', function (e) {
    var type = e.layerType;
    var drawlayer = e.layer;

    //Make global variable for stamp
    poly_id = L.stamp(drawlayer);

    // Do whatever else you need to. (save to db, add to map etc)
    drawnItems.addLayer(drawlayer);
    drawnItems.bringToBack()
    setSomeInactive(poly_id);
});

map.on('draw:editstart', function (e) {
    setAllActive();
});

map.on('draw:edited', function (e) {
    setSomeInactive(poly_id);
});

map.on('draw:deleted', function(e) {
    setAllActive();
});





// var test_query = isMarkerInsidePolygon(events["p000k24t"], drawnItems)


    // Work out which earthquakes are within polygon
    // _.forEach(events, function (value, key) {
    //     setMarkerInactive(value);
    // });
// });


//     _.forEach(events, function(value, key) {
//        setMarkerInactive(value)
////        if isMarkerInsidePolygon(value.marker, layer) {
////            // Do something with points in polygon
////        } else {
////            setMarkerInactive(value)
////        }
//    });
// });