# README #

Desktop GUI (Graphical User Interface) for analysing and viewing time series data stored in ASDF files.

[ASDF Landing Page](http://seismic-data.org)

### Dependencies

**The GUI has the following dependencies:**

* `Python 3.6.2`
* `PyQt5 5.9.2`
* `qdarkstyle 2.5.4`
* `Pandas 0.23.4`
* `Pyqtgraph 0.10.0`
* `obspy 1.1.0`
* `pyasdf 1.1.0`

**To Install the dependencies using Anaconda:**

* `conda create -n {desired environment name} python=3.6.2 pyqt=5.9.2`

* `source activate {desired environment name}`

* `conda install pandas`

* `pip install qdarkstyle==2.5.4`

* `conda install pyqtgraph`

* `conda install obspy=1.1.0 -c conda-forge`

* `conda install -c conda-forge pyasdf`