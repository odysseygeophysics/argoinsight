from PyQt5.QtGui import QIcon
import os


class SetButtonIcons(object):
    def __init__(self):
        pass

    def set_button_icons(self):
        icons_dir = os.path.abspath(os.path.join(
            os.path.dirname(__file__), "../../icons"))

        self.ui.selection_options_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "Logo_justglobe_transparent.png")))
        self.ui.eq_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-line-graphic.png")))
        self.ui.cloud_link_pushButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-download-from-the-cloud.png")))
        # self.ui.time_selection_toolButton.setIcon(
        #     QIcon(os.path.join(icons_dir, "icons8-time-span.png")))
        self.ui.data_select_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-check-mark-symbol.png")))
        self.ui.hierarchy_view_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-hierarchy.png")))
        self.ui.temp_toolButton.setIcon(
            QIcon())
        self.ui.filter_selection_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-filter.png")))

        self.ui.reset_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-reset.png")))
        self.ui.previous_view_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-go-back.png")))
        self.ui.previous_time_jump_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-skip-to-start.png")))
        self.ui.next_time_jump_toolButton.setIcon(
            QIcon(os.path.join(icons_dir, "icons8-skip-this-track.png")))
        # self.ui.filter_checkBox.setIcon(
        #     QIcon(os.path.join(icons_dir, "icons8-change.png")))
        # self.ui.normalise_checkBox.setIcon(
        #     QIcon(os.path.join(icons_dir, "icons8-normal-distribution-histogram.png")))
        # self.ui.mini_detrend_demean_checkBox.setIcon(
        #     QIcon(os.path.join(icons_dir, "icons8-decline.png")))
        # self.ui.custom_filter_toolButton.setIcon(
        #     QIcon(os.path.join(icons_dir, "icons8-customize-2.png")))
        # self.ui.export_toolButton.setIcon(
        #     QIcon(os.path.join(icons_dir, "icons8-export.png")))



