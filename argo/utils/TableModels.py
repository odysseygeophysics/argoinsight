from PyQt5 import QtWidgets, QtCore


class HoldingsPandasModel(QtCore.QAbstractTableModel):
    """
    Class to populate holdings table view with a pandas dataframe
    """

    def __init__(self, parent=None, *args):
        super(HoldingsPandasModel, self).__init__()
        self.tableView = parent
        self.datatable = None
        self.headerdata = None

    def update(self, dataIn):
        # print('Updating Model')
        self.datatable = dataIn
        # print('Datatable : {0}'.format(self.datatable))
        headers = dataIn.columns.values
        header_items = [
            str(field)
            for field in headers
        ]
        self.headerdata = header_items
        # print('Headers')
        # print(self.headerdata)

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.datatable.index)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.datatable.columns.values)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            i = index.row()
            j = index.column()
            return QtCore.QVariant('{0}'.format(self.datatable.iat[i, j]))
        else:
            return QtCore.QVariant()

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        if index.column() == 0:
            # print("set_data")
            # print(value)
            self.datatable.iat[index.row(), 0] = value
            print(self.datatable)
            self.tableView.holdingsTableItemChecked.emit(self.datatable)
            return value
        return value

    def headerData(self, col, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return '{0}'.format(self.headerdata[col])

    def flags(self, index):
        if index.column() == 0:
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled
        else:
            return QtCore.Qt.ItemIsEnabled


class HoldingsCheckBoxDelegate(QtWidgets.QStyledItemDelegate):
    """
    A delegate that places a fully functioning QCheckBox in every
    cell of the column to which it's applied
    """
    def __init__(self, parent):
        QtWidgets.QItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        '''
        Important, otherwise an editor is created if the user clicks in this cell.
        ** Need to hook up a signal to the model
        '''
        return None

    def paint(self, painter, option, index):
        '''
        Paint a checkbox without the label.
        '''

        # print("....")
        # print(index.data())
        # print(type(index.data()))
        check_box_style_option = QtWidgets.QStyleOptionButton()
        check_box_style_option.state |= QtWidgets.QStyle.State_Active


        if index.flags() & QtCore.Qt.ItemIsEditable:
            check_box_style_option.state |= QtWidgets.QStyle.State_Enabled
        else:
            check_box_style_option.state |= QtWidgets.QStyle.State_ReadOnly

        if index.data() == "True":
            check_box_style_option.state |= QtWidgets.QStyle.State_On
        elif index.data() == "False":
            check_box_style_option.state |= QtWidgets.QStyle.State_Off

        check_box_style_option.rect = self.getCheckBoxRect(option)

        check_box_style_option.state |= QtWidgets.QStyle.State_Enabled

        QtWidgets.QApplication.style().drawControl(QtWidgets.QStyle.CE_CheckBox, check_box_style_option, painter)



    def editorEvent(self, event, model, option, index):
        '''
        Change the data in the model and the state of the checkbox
        if the user presses the left mousebutton or presses
        Key_Space or Key_Select and this cell is editable. Otherwise do nothing.
        '''
        # print('Check Box editor Event detected : ')
        # print(event.type())
        if not index.flags() & QtCore.Qt.ItemIsEditable:
            return False

        # print('Check Box editor Event detected : passed first check')
        # Do not change the checkbox-state
        if event.type() == QtCore.QEvent.MouseButtonPress:
          return False
        if event.type() == QtCore.QEvent.MouseButtonRelease or event.type() == QtCore.QEvent.MouseButtonDblClick:
            if event.button() != QtCore.Qt.LeftButton or not self.getCheckBoxRect(option).contains(event.pos()):
                return False
            if event.type() == QtCore.QEvent.MouseButtonDblClick:
                return True
        elif event.type() == QtCore.QEvent.KeyPress:
            if event.key() != QtCore.Qt.Key_Space and event.key() != QtCore.Qt.Key_Select:
                return False
        else:
            return False

        # Change the checkbox-state
        self.setModelData(None, model, index)
        return True

    def setModelData(self, editor, model, index):
        '''
        The user wanted to change the old state in the opposite.
        '''
        # print('SetModelData')
        if index.data() == "True":
            newValue = "False"

        elif index.data() == "False":
            newValue = "True"

        # print('New Value : {0}'.format(newValue))
        model.setData(index, newValue, QtCore.Qt.EditRole)

    def getCheckBoxRect(self, option):
        check_box_style_option = QtWidgets.QStyleOptionButton()
        check_box_rect = QtWidgets.QApplication.style().subElementRect(QtWidgets.QStyle.SE_CheckBoxIndicator, check_box_style_option, None)
        check_box_point = QtCore.QPoint (option.rect.x() +
                            option.rect.width() / 2 -
                            check_box_rect.width() / 2,
                            option.rect.y() +
                            option.rect.height() / 2 -
                            check_box_rect.height() / 2)
        return QtCore.QRect(check_box_point, check_box_rect.size())