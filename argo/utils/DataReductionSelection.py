import pandas as pd
import pyasdf
from obspy import Stream, UTCDateTime
from argo.database.seisds import SeisDB
from collections import defaultdict


def open_asdf_files(self):
    holdings_table_df = self.holdings_table_current_df
    # get filenames and paths for selected ASDF files
    sel_file_df = holdings_table_df.where(holdings_table_df["Selection"] == "True").dropna()

    # set up new dictionary for file handlers and database for selected ASDF files
    self.ASDF_accessor = {}

    for index, row in sel_file_df.iterrows():
        asdf_file = pyasdf.ASDFDataSet(row["Path"])
        jsondb_file_path = row["Path"].split(".")[0] + "_raw_dataDB.json"

        print(jsondb_file_path)
        # load in JSON DB file using SeisDS
        # create the seismic database
        seisdb = SeisDB(json_file=jsondb_file_path)

        self.ASDF_accessor[row["Type"] + "|" + row["Filename"]] = {"ds": asdf_file, "db": seisdb}


def get_current_holdings_df(self):
    table_holdings_pandas_dict = self.holdingsDB.holdings_pandas_dict
    # print(self.holdings_tree_selection_dict)

    relevant_df_list = []

    for surv_virt_net, selection_status in self.holdings_tree_selection_dict.items():
        # check if the surv_virt_net is checked
        if selection_status == "True":
            # print(surv_virt_net)
            relevant_df_list.append(
                table_holdings_pandas_dict[surv_virt_net.split("|")[0]][surv_virt_net.split("|")[1]])

    if len(relevant_df_list) == 0:
        # no holdings for selected src_dir and virt network draw empty table
        empty_df = pd.DataFrame(data=None, columns=["Selection", "Filename", 'Path', "Type"])
        self.holdings_table_current_df = empty_df

    else:
        self.holdings_table_current_df = pd.concat(relevant_df_list, ignore_index=True)

        # ignore duplicate entries (i.e. a file is associated with multiple virtual networks
        self.holdings_table_current_df.drop_duplicates(inplace=True)


def get_current_metadata_df(self):
    holdings_table_df = self.holdings_table_current_df
    meta_holdings_pandas_dict = self.metaDB.holdings_pandas_dict

    # get list of selected files
    sel_file_df = holdings_table_df.where(holdings_table_df["Selection"] == "True").dropna()

    # print(sel_file_df["Filename"])

    relevant_df_list = []

    for index, row in sel_file_df.iterrows():
        relevant_df_list.append(meta_holdings_pandas_dict[row["Type"]][row["Filename"]])

    if len(relevant_df_list) == 0:
        # no holdings for selected src_dir and virt network draw empty table
        self.meta_current_df = pd.DataFrame(data=None)

    else:
        self.meta_current_df = pd.concat(relevant_df_list, ignore_index=True)


def get_selected_metadata(self):
    # print("================")
    # print(self.holdings_tree_selection_dict)
    # get selected Survey set
    self.sel_surv_set = set()
    for surv_vnet, bool_val in self.holdings_tree_selection_dict.items():
        if bool_val == "True":
            self.sel_surv_set.add(surv_vnet.split("|")[0])

    # get selected filenames:
    self.sel_filename_list = self.meta_current_df["Filename"].drop_duplicates().tolist()

    # get lists of checked codes
    self.selected_NSCL_list_dict = {}

    for header in self.selected_NSCL_df.columns:
        temp_series = self.selected_NSCL_df[header].drop_duplicates()
        # make list and append to dict
        self.selected_NSCL_list_dict[header] = temp_series.tolist()


def get_nscl_level_recording_intervals(self):
    """
    Function to get a recording intervals information fo rcurrnetly selected stations
    """

    self.metaDB.queryByNSCL(net=self.selected_NSCL_list_dict["Network"],
                            sta=self.selected_NSCL_list_dict["Station"],
                            chan=self.selected_NSCL_list_dict["Channel"],
                            loc=self.selected_NSCL_list_dict["Location"],
                            surv_list=list(self.sel_surv_set),
                            file_list=self.sel_filename_list)

    # print("----- After Query -----")
    # print(self.metaDB.query_ret_dict)
    # now get recording intervals based on selected NSCL's
    self.metaDB.get_NSCL_recording_intervals()


def get_detailed_recording_intervals(self):
    """
    Function to get detailed recording intervals (to wavefomr level for selected NSCL's
    """

    self.detailed_recording_intervals = {}
    self.sel_chan_loc_set = set()

    # go through and get intervals
    for surv_name in self.sel_surv_set:
        for file_name in self.sel_filename_list:
            # get the ASDF file handler and JSON DB
            print("Working on: ", surv_name, file_name)
            ds = self.ASDF_accessor[surv_name + "|" + file_name]["ds"]
            db = self.ASDF_accessor[surv_name + "|" + file_name]["db"]

            self.detailed_recording_intervals[surv_name + "|" + file_name] = {}

            print("Extracting Data Interval information for: ", self.selected_NSCL_df)

            # go through rows of df
            for index, row in self.selected_NSCL_df.iterrows():
                print("Getting rec int for: ", row)
                # get recording interval for NSCL
                rec_int_array = db.get_recording_intervals(net=[row["Network"]],
                                           sta=[row["Station"]],
                                           chan=[row["Channel"]],
                                           loc=[row["Location"]],
                                           tags=["raw_recording"])

                # print(rec_int_array)

                if not rec_int_array is None:
                    # if it is none then there is no data for the desired net,sta,chan,loc,tags

                    if not row["Network"]+"_"+row["Station"] in self.detailed_recording_intervals[surv_name + "|" + file_name].keys():
                        self.detailed_recording_intervals[surv_name + "|" + file_name][row["Network"]+"_"+row["Station"]] = {}

                    self.detailed_recording_intervals[surv_name + "|" + file_name][row["Network"]+"_"+row["Station"]][row["Channel"]+"_"+row["Location"]] =  rec_int_array
                    # add teh chan_loc pair into the chan_loc set to get unique chan_loc combinations
                    self.sel_chan_loc_set.add(row["Channel"]+"_"+row["Location"])

    # print("retreived Detailed rec intervals")
    # print(self.detailed_recording_intervals)

def query_to_stream(self, ds, query, interval_tuple):
    """
    method to use output from seisds query to return waveform streams
    """

    # Open a new st object
    st = Stream()

    for matched_info in query.values():
        # self.db.retrieve_full_db_entry(matched_info["ASDF_tag"])

        # read the data from the ASDF into stream
        temp_tr = ds.waveforms[matched_info["new_network"].decode('utf-8') + '.' + matched_info["new_station"].decode('utf-8')][
            matched_info["ASDF_tag"]][0]

        # trim trace to start and endtime
        temp_tr.trim(starttime=UTCDateTime(interval_tuple[0]), endtime=UTCDateTime(interval_tuple[1]))

        # append the asdf id tag into the trace stats so that the original data is accessible
        temp_tr.stats.asdf.orig_id = matched_info["ASDF_tag"]

        # # if the channel is BKO i.e. temp channel then divde by 1 million
        # if temp_tr.stats.channel ==

        # append trace to stream
        st += temp_tr

        # free memory
        temp_tr = None

    try:

        if st.__nonzero__():
            # Attempt to merge all traces with matching ID'S in place
            print('')
            print('Merging Traces from %s Stations....' % len(st))
            # filling no data with 0
            st.merge(fill_value=0)
            print('\nTrimming Traces to specified time interval....')
            st.trim(starttime=UTCDateTime(interval_tuple[0]), endtime=UTCDateTime(interval_tuple[1]))

            # if the channel is BKO i.e. temp channel then divde by 1 million
            temp_channels_st = st.select(channel="*KO")

            for tr in temp_channels_st:
                tr.normalize(1000000)


            return st
        else:
            return None
    except UnboundLocalError:
        # the station selection dialog box was cancelled
        return None

#TODO: threading so that data loading can happen independantly to GUI control (to stop freezing)
#TODO: keep cache of loaded Stream objects up to maximum level to allow for quicker in-stream moving # (i.e. if desired time interval is within stream boundaries that is already loaded in)
def extract_waveforms_frm_ASDF(self, **kwargs):
    # Extract all relevant traces into single stream object
    # get selected traces.


    # container for streams for all files
    self.st = Stream()

    # check the size of the region
    roi_region = self.time_slider_roi[1]-self.time_slider_roi[0]

    #TODO: fix up condtion for size of desired time span to plot (i.e. dont allow plotting if the span is tyoo long and smapling rate too high for waveforms)
    if roi_region > 60*60*24*3:
        return


    # go through and get intervals
    for surv_name in self.sel_surv_set:
        for file_name in self.sel_filename_list:
            # get the ASDF file handler and JSON DB
            print("Working on: ", surv_name, file_name)
            ds = self.ASDF_accessor[surv_name + "|" + file_name]["ds"]
            db = self.ASDF_accessor[surv_name + "|" + file_name]["db"]

            print(ds)
            print(db)

            print("Quering DB with")
            print(self.selected_NSCL_list_dict)
            print(self.time_slider_roi)

            # go through rows of selected df
            for index, row in self.selected_NSCL_df.iterrows():
                print("Getting data for: ", row)

                st_temp = ds.get_waveforms(network=row["Network"], station=row["Station"], location=row["Location"],
                                           channel=row["Channel"], starttime=UTCDateTime(self.time_slider_roi[0]),
                                           endtime=UTCDateTime(self.time_slider_roi[1]), tag="raw_recording", automerge=True)

                if not st_temp == None:
                    self.st += st_temp

                st_temp = None

    print(self.st)
