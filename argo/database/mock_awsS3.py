import json
import pandas as pd
from collections import defaultdict


class holdingsDB(object):
    """
   Holdings database object for JSON  holdings database
   """

    def __init__(self, json_file=False):
        self._json_loaded = False
        if json_file:
            try:
                f = open(json_file, 'r')
                self._json_dict = json.load(f)
                self._json_file = json_file
                self._json_loaded = True
                self.generateIndex()

            except IOError as e:
                print("I/O error({0}): {1}".format(e.errorno, e.strerror))
            except ValueError as e:
                print("JSON Decoding has failed with a value error({0}): {1}".format(e.errorno, e.strerror))

    def generateIndex(self):
        assert self._json_loaded, "Invalid SeisDB object. Try loading a valid JSON file first."

        try:
            # dictionary with keys as integer index (corresponding to numpy array indexes) and value which is filename of ASDF file
            self._indexed_dict = {}
            # new dictionary to be sure that starttime and endtime fields are float
            self._formatted_dict = {}
            for _i, (key, value) in enumerate(self._json_dict.items()):
                self._indexed_dict[_i] = key
                temp_dict = {}
                for _j, (sub_key, sub_value) in enumerate(value.items()):
                    temp_dict[sub_key] = sub_value

                self._formatted_dict[_i] = temp_dict
            self._valid_index = True
        except KeyError as e:
            print("Indexing JSON dictionary has failed with a key error({0}): {1}".format(e.errorno, e.strerror))


    def generate_data(self):
        """
        Generator for all data in DB
        :return:
        """
        # first iterate through directories (i.e. Surveys, Processing etc..)
        for key, value in self._json_dict.items():
            # second iterate through ASDF files associated with directory
            for sub_key, sub_value in value.items():
                yield key, sub_key, sub_value


    def database_to_pandas(self):
        """
        Method to turn database into pandas dataframe

        :return:
        """

        # create empty dict for dictionary of pandas tables relating to particular directories and virtual networks
        holdings_def_dict = {}

        # go through database
        for _i, holding_entry in enumerate(self.generate_data()):

            src_dir = holding_entry[0]
            filename = holding_entry[1]
            file_info = holding_entry[2]


            # create a new default dict for directory if doesnt exist
            if not src_dir in holdings_def_dict.keys():
                holdings_def_dict[src_dir] = defaultdict(list)

            # get the list of virtual networks the filename is associated with
            virt_net_list = file_info["VirtNet"]

            for virt_net in virt_net_list:
                holdings_def_dict[src_dir][virt_net].append([filename, file_info["Path"]])


        self.holdings_pandas_dict = {}
        # now go through defaultdict and create pandas df's

        for src_dir, virt_net_dict in holdings_def_dict.items():
            # print(src_dir, virt_net_dict)

            # add src_dir to dict if not exits
            if not src_dir in self.holdings_pandas_dict.keys():
                self.holdings_pandas_dict[src_dir] = {}

            for virt_net in virt_net_dict.keys():

                holding_entry_list = holdings_def_dict[src_dir][virt_net]

                # create empty data frame
                db_df = pd.DataFrame(data=None, columns=["Selection", "Filename", 'Path', "Type"])

                for _j, holding in enumerate(holding_entry_list):
                    db_df.loc[_j] = ["False", holding[0], holding[1], src_dir]

                db_df.reset_index(drop=True, inplace=True)



                self.holdings_pandas_dict[src_dir][virt_net] = db_df

                # print(src_dir, virt_net)
                # print(db_df)






