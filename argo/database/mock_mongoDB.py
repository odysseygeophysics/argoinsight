import json
import pandas as pd
import numpy as np


class metaDB(object):
    """
      Metadata database object for JSON  metadata database
      """

    def __init__(self, json_file=False):
        self._json_loaded = False
        if json_file:
            try:
                f = open(json_file, 'r')
                self._json_dict = json.load(f)
                self._json_file = json_file
                self._json_loaded = True
                self.generateIndex()

            except IOError as e:
                print("I/O error({0}): {1}".format(e.errorno, e.strerror))
            except ValueError as e:
                print("JSON Decoding has failed with a value error({0}): {1}".format(e.errorno, e.strerror))

    def generateIndex(self):
        assert self._json_loaded, "Invalid SeisDB object. Try loading a valid JSON file first."

        try:
            self._surv_dict = {}
            # first level of dict will be survey type
            for surv_type in self._json_dict.keys():
                file_dict = self._json_dict[surv_type]
                _file_dict = {}

                for file_name in file_dict.keys():
                    meta_dict = file_dict[file_name]

                    # dictionary with keys as integer index (corresponding to numpy array indexes) and value which is NSCL tag
                    _indexed_dict = {}
                    # List for numpy array building
                    _index_dict_list = []
                    # container for dtypes
                    type_list = []
                    # check if the dtype has been populated
                    dtype_pop = False
                    for _i, (key, value) in enumerate(meta_dict.items()):
                        _indexed_dict[_i] = key
                        temp_list = []

                        # print(value)
                        # print(type(value))

                        for sub_key, sub_value in value.items():
                            # print(sub_key, sub_value)

                            # only add some of the attributes to the numpy array to speed up lookup
                            if sub_key == "StartDate":
                                temp_list.append(float(sub_value))
                                if not dtype_pop:
                                    type_list.append(('st', float))
                            elif sub_key == "EndDate":
                                temp_list.append(float(sub_value))
                                if not dtype_pop:
                                    type_list.append(('et', float))
                            elif sub_key == "Network":
                                temp_list.append(str(sub_value))
                                if not dtype_pop:
                                    type_list.append(('net', 'S2'))
                            elif sub_key == "Station":
                                temp_list.append(str(sub_value))
                                if not dtype_pop:
                                    type_list.append(('sta', 'S5'))
                            elif sub_key == "Channel":
                                temp_list.append(str(sub_value))
                                if not dtype_pop:
                                    type_list.append(('cha', 'S3'))
                            elif sub_key == "Location":
                                temp_list.append(str(sub_value))
                                if not dtype_pop:
                                    type_list.append(('loc', 'S2'))
                            elif sub_key == "Latitude":
                                temp_list.append(float(sub_value))
                                if not dtype_pop:
                                    type_list.append(('lat', float))
                            elif sub_key == "Longitude":
                                temp_list.append(float(sub_value))
                                if not dtype_pop:
                                    type_list.append(('lon', float))
                            elif sub_key == "Elevation":
                                temp_list.append(float(sub_value))
                                if not dtype_pop:
                                    type_list.append(('elev', float))

                        dtype_pop = True

                        _index_dict_list.append(tuple(temp_list))

                    dt = np.dtype(type_list)
                    _indexed_np_array = np.array(_index_dict_list, dtype=dt)

                    _file_dict[file_name] = (_indexed_dict, _indexed_np_array)

                self._surv_dict[surv_type] = _file_dict
                self._valid_index = True
        except KeyError as e:
            print("Indexing JSON dictionary has failed with a key error({0}): {1}".format(e.errorno, e.strerror))


    def queryByNSCL(self, surv_list, file_list, net, sta, chan, loc):
        assert self._json_loaded, "Invalid SeisDB object. Try loading a valid JSON file first."
        assert self._valid_index, "Invalid SeisDB object. Index has not been generated."

        # print("Quering by NSCL")

        # encode the string list into byte liusts
        b_net = [x.encode('utf-8') for x in net]
        b_sta = [x.encode('utf-8') for x in sta]
        b_chan = [x.encode('utf-8') for x in chan]
        b_loc = [x.encode('utf-8') for x in loc]

        self.query_ret_dict = {}

        for surv_name in surv_list:
            for file_name in file_list:

                try:
                    # access the numpy array for surv and file
                    # print("Accessed Numpy Index Array for: ", surv_name, file_name)
                    _indexed_np_array = self._surv_dict[surv_name][file_name][1]
                    _indexed_dict = self._surv_dict[surv_name][file_name][0]


                    _indexed_np_array_masked = np.where(
                        (np.in1d(_indexed_np_array['net'], b_net)) & (np.in1d(_indexed_np_array['sta'], b_sta))
                        & (np.in1d(_indexed_np_array['cha'], b_chan)) & (
                            np.in1d(_indexed_np_array['loc'], b_loc)))

                    # print(_indexed_np_array_masked)


                    if not surv_name in self.query_ret_dict.keys():
                        self.query_ret_dict[surv_name] = {}

                    if not file_name in self.query_ret_dict[surv_name].keys():
                        self.query_ret_dict[surv_name][file_name] = _indexed_np_array_masked

                    self.query_ret_dict[surv_name][file_name] = _indexed_np_array_masked

                except KeyError:
                    continue
        return self.query_ret_dict

    def get_NSCL_recording_intervals(self, surv_dict=None, query_ret_dict=None):
        # print("============================================================")
        # print("Getting Recording Intervals")

        if not query_ret_dict == None:
            self.query_ret_dict = query_ret_dict

        if not surv_dict == None:
            self._surv_dict = surv_dict

        # go through and get intervals
        for surv_name in self.query_ret_dict.keys():
            for file_name in self.query_ret_dict[surv_name].keys():
                # print("Working on: ", surv_name, file_name)
                _indexed_np_array_masked = self.query_ret_dict[surv_name][file_name]

                _masked_np_array = self._surv_dict[surv_name][file_name][1][_indexed_np_array_masked]

                if _masked_np_array.shape[0]==0:
                    #i.e. empty ignore
                    continue

                # print("Selected data: ")
                # print(_masked_np_array)

                _st_array = _masked_np_array["st"]
                _et_array = _masked_np_array["et"]

                # _arg_sorted_st_array = np.argsort(_st_array)


                # _sorted_st_array = _st_array[_arg_sorted_st_array]
                # _sorted_et_array = _et_array[_arg_sorted_st_array]
                _sorted_st_array = np.sort(_st_array)
                _sorted_et_array = np.sort(_et_array)

                # print(_sorted_st_array)
                # print(_sorted_et_array)

                if _sorted_st_array.shape[0] == 1:
                    # i.e. there are no gaps
                    # print(_sorted_st_array[0])
                    return (np.array([[_sorted_st_array[0]], [_sorted_et_array[0]]]))

                # offset the starttime array so that we start from the second recorded waveform
                _offset_st_array = _sorted_st_array[1:]

                # print(_offset_st_array)

                _diff_array = _offset_st_array - _sorted_et_array[:-1]

                # print(_diff_array)

                # now get indexes when gap (diff positive) or overlap (diff negative)
                # remember to add 1 to index because of offset
                _sorted_gaps_index = np.where(_diff_array > 1)

                # print(_sorted_gaps_index)

                # if it is empty then the gaps are smaller than 1 second ignore them
                if _sorted_gaps_index[0].shape[0] == 0:
                    self.rec_int_array = np.array([[_sorted_st_array[0]], [_sorted_et_array[-1]]])
                    return

                # recording intervals:
                # rec_start_list_after gaps = list(_sorted_st_array[_sorted_gaps_index])
                rec_end_list_bef_gaps = list(_sorted_et_array[_sorted_gaps_index])

                # print(rec_end_list_bef_gaps)

                rec_start_list = []
                rec_end_list = []

                int_id = 0
                for _i in range(len(rec_end_list_bef_gaps)):
                    rec_start_list.append(_sorted_st_array[int_id])
                    rec_end_list.append(rec_end_list_bef_gaps[_i])
                    int_id = _sorted_gaps_index[0][_i] + 1
                    if _i == len(rec_end_list_bef_gaps) - 1:
                        # last interval
                        # print(_sorted_st_array[int_id], _sorted_et_array[-1])
                        rec_start_list.append(_sorted_st_array[int_id])
                        rec_end_list.append(_sorted_et_array[-1])


                self.rec_int_array = np.array([rec_start_list, rec_end_list])

                # print(self.rec_int_array)

                # print(self.rec_int_array[0][0])
                # print(self.rec_int_array[1][-1])



    def retrieve_full_db_entry(self, json_key):
        """
        Method to take an output from queryByTime and get the full information from the original JSON db
        :return: full DB
        """

        # for
        # print(self._json_dict[json_key])
        return (self._json_dict[json_key])

    def generate_data(self):
        """
        Generator for all data in DB
        :return:
        """
        # first iterate through directories (i.e. Surveys, Processing etc..)
        for key, value in self._json_dict.items():
            # second iterate through ASDF files associated with directory
            for sub_key, sub_value in value.items():
                yield key, sub_key, sub_value

    def database_to_pandas(self):
        """
        Method to turn database into pandas dataframe

        :return:
        """

        # create empty dict for dictionary of pandas tables relating to particular directories and virtual networks
        holdings_def_dict = {}

        # go through database
        for _i, holding_entry in enumerate(self.generate_data()):

            src_dir = holding_entry[0]
            filename = holding_entry[1]
            file_meta = holding_entry[2]

            # print(src_dir)
            # print(filename)
            # print(file_meta)

            # create a new default dict for directory if doesnt exist
            if not src_dir in holdings_def_dict.keys():
                holdings_def_dict[src_dir] = {}

            if not filename in holdings_def_dict[src_dir].keys():
                holdings_def_dict[src_dir][filename] = []

            # go through channels in file meta:
            for channel_id in file_meta.keys():
                # add channel_id into dict
                chan_dict = file_meta[channel_id]
                chan_dict["chan_id"] = channel_id

                #add filename into dict
                chan_dict["Filename"] = filename
                holdings_def_dict[src_dir][filename].append(chan_dict)

        # print(holdings_def_dict)

        self.holdings_pandas_dict = {}
        # now go through defaultdict and create pandas df's

        for src_dir, filename_dict in holdings_def_dict.items():
            # add filename to dict if not exits
            if not src_dir in self.holdings_pandas_dict.keys():
                self.holdings_pandas_dict[src_dir] = {}

            for filename, chan_meta_list in filename_dict.items():

                holding_entry_list = holdings_def_dict[src_dir][filename]



                # create empty data frame
                db_df = pd.DataFrame(data=None, columns=["chan_id", "Filename", "Network",
                                                         "Station",
                                                         "Channel",
                                                         "Location",
                                                         "Latitude",
                                                         "Longitude",
                                                         "Elevation",
                                                         "StartDate",
                                                         "EndDate",
                                                         "SampleRate",
                                                         "Azimuth",
                                                         "Dip"])

                for _j, holding in enumerate(holding_entry_list):
                    db_df.loc[_j] = [holding["chan_id"], holding["Filename"], holding["Network"],
                                                         holding["Station"],
                                                         holding["Channel"],
                                                         holding["Location"],
                                                         holding["Latitude"],
                                                         holding["Longitude"],
                                                         holding["Elevation"],
                                                         holding["StartDate"],
                                                         holding["EndDate"],
                                                         holding["SampleRate"],
                                                         holding["Azimuth"],
                                                         holding["Dip"]]

                db_df.reset_index(drop=True, inplace=True)

                self.holdings_pandas_dict[src_dir][filename] = db_df

                # print(src_dir, filename)
                # print(db_df)





def main():
    indexed_dict = {0: "OA.AA01.BHZ", 1: "OA.AA02.BHZ", 2: "OA.AA03.BHZ",
                    3: "OA.AA04.BHZ", 4: "OA.AA05.BHZ", 5: "OA.AA06.BHZ",
                    6:"OA.AA07.BHZ", 7: "OA.AA08.BHZ", 8: "OA.AA09.BHZ"}


    dt = np.dtype([("st", float), ("et", float)])

    indexed_np_array = np.array([(1.0, 5.5),
                                 (5.0,6.0),
                                 (4.0,7.2),
                                 (3.0,6.5),
                                 (6.7,7.0),
                                 (11.0,16.0),
                                 (10.8,13.0),
                                 (14.0,20.0),
                                 (22.0,30.0)], dtype=dt)

    # test_array = np.array([float(1), float(2)], dtype=np.dtype([("st", "f8"), ("et", "f8")]))

    indexed_np_array_mask = np.array([0,1,2,3,4,5,6,7,8],)


    # print(indexed_np_array[indexed_np_array_mask])

    surv_dict = {"Surveys": {"OA.h5": (indexed_dict, indexed_np_array)}}
    query_ret_dict ={"Surveys": {"OA.h5": indexed_np_array_mask}}

    mdb = metaDB()

    # now test the recording intervals method
    mdb.get_NSCL_recording_intervals(surv_dict = surv_dict, query_ret_dict=query_ret_dict)


if __name__ == '__main__':
    main()