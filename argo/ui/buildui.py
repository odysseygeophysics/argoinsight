from PyQt5 import QtWidgets
from PyQt5 import QtCore
from functools import partial
import os
import pandas as pd
from obspy import read, UTCDateTime
import numpy as np

from argo.utils.DataReductionSelection import get_current_holdings_df
from argo.widgets.WaveformPlotWidget import WaveformPlotForm
from argo.widgets.DataAvailabilityPlotWidget import DataAvailabilityPlotForm



def build_holdings_tree_view(self):
    # set up dictionary for selected tree items
    self.holdings_tree_selection_dict = {}

    holdings_pandas_dict = self.holdingsDB.holdings_pandas_dict
    self.ui.holdings_treeWidget.updateHoldingsTree(holdings_pandas_dict, self.holdings_tree_selection_dict)
    # connect the signals
    self.connectHoldingsTreeWidget()


def build_holdings_table_view(self):
    # get the current holdings df based on selection in the holdings tree widget
    get_current_holdings_df(self)
    holdings_table_current_df = self.holdings_table_current_df
    # update the holdings table
    self.ui.holdings_tableView.updateHoldingsTable(holdings_table_current_df)
    # connect the signals
    self.connectHoldingsTableView()

def build_time_range_slider(self, **kwargs):
    print("build_time_range_slider")
    print(kwargs)
    # Boolean to capyture when the recording intervals in the time slider have been modified
    mod_rec_array_bool = False

    # update the range slider values with new values (or current values in time_range_slider_dict if no values provided
    for kwarg_key, kwarg_val in kwargs.items():
        # update the slider values dict with supplied kwargs if the kwarg_key is the same as the slider values dict keys
        if kwarg_key in self.time_range_slider_values_dict:
            self.time_range_slider_values_dict[kwarg_key] = kwarg_val

            if kwarg_key == "rec_array":
                mod_rec_array_bool = True
                earliest = kwarg_val[0][0]
                latest = kwarg_val[1][-1]

                # calculate the initial start and end for the roi
                total_time = latest - earliest
                # time in seconds that is 10 percent of the region covered by the earliest and latest times from recording intervals
                frac_time = total_time * 0.1

                self.time_range_slider_values_dict["roi_st"] = earliest + frac_time
                self.time_range_slider_values_dict["roi_et"] = latest - frac_time



                # set the earliest rec interval starttime
                self.time_range_slider_values_dict["st"] = earliest
                self.time_range_slider_values_dict["et"] = latest


    print("Updating Time Slider With:")
    print(self.time_range_slider_values_dict)
    slider_dict = self.time_range_slider_values_dict

    # now update the time slider (re-plot with modified rec intervals)
    if mod_rec_array_bool:
        self.ui.time_range_slider_widget.update_time_slider_data(rec_intervals_array=slider_dict["rec_array"], st=slider_dict["st"], et=slider_dict["et"])

    self.ui.time_range_slider_widget.set_time_slider_region(slider_dict["roi_st"], slider_dict["roi_et"])


def data_availability_plot_update(self):
    # remove all existing plots from list widget
    self.ui.data_availability_listWidget.clear()

    print("Building Data Availability plot with: ")
    print(self.detailed_recording_intervals)

    # set the roi progress to active (i.e. we should make use of the signals
    self.data_avail_roi_progress = "active"

    # create new list entry for each surv/filename
    for surv_filename in self.detailed_recording_intervals.keys():
        # print("Working on: ", net_sta)

        net_sta_array_dict = self.detailed_recording_intervals[surv_filename]

        # set up widget for net/sta
        data_avail_item = QtWidgets.QListWidgetItem(self.ui.data_availability_listWidget)

        self.data_avail_item_widget = DataAvailabilityPlotForm()

        self.data_avail_item_widget.plotDataAvailData(net_sta_array_dict, self.sel_chan_loc_set)

        data_avail_item.setSizeHint(self.data_avail_item_widget.sizeHint())
        self.ui.data_availability_listWidget.addItem(data_avail_item)

        self.ui.data_availability_listWidget.setItemWidget(data_avail_item, self.data_avail_item_widget)

        self.connectDataAvailPlot(self.data_avail_item_widget)

def data_availability_plot_range_update(self, active_data_avail_widget=None):
    def iterAllItems(self):
        for i in range(self.count()):
            yield self.itemWidget(self.item(i))


    # set the region of lri
    for data_avail_widget in iterAllItems(self.ui.data_availability_listWidget):

        # check that the widget is not the active data_avail_widget
        if not data_avail_widget == active_data_avail_widget:
            print("Setting Widget ROI....")
            data_avail_widget.set_time_slider_region(self.time_slider_roi[0], self.time_slider_roi[1])




def waveform_plot_update(self):
    print("waveform_plot_update")

    # remove all existing plots from list widget
    self.ui.waveform_listWidget.clear()

    # st_test = read("tests/test_resources/stream_test.mseed")
    # # print(st_test)

    # ignore if there are no traces in stream
    if len(self.st) == 0:
        return

    unique_net_sta = set()

    for tr in self.st:
        tr_id = tr.get_id()
        elems = tr_id.split(".")
        unique_net_sta.add(elems[0] + "." + elems[1])

    # print(unique_net_sta)

    # add plot widget for each station to listwidget

    for net_sta in unique_net_sta:
        # print("Working on: ", net_sta)

        # get stream with traces just for this net/sta
        sel_st = self.st.select(network=net_sta.split(".")[0], station=net_sta.split(".")[1])

        # print(sel_st)

        # set up widget for net/sta
        waveform_item = QtWidgets.QListWidgetItem(self.ui.waveform_listWidget)

        waveform_item_widget = WaveformPlotForm()

        waveform_item_widget.plotWaveformData(sel_st)

        waveform_item.setSizeHint(waveform_item_widget.sizeHint())
        self.ui.waveform_listWidget.addItem(waveform_item)

        self.ui.waveform_listWidget.setItemWidget(waveform_item, waveform_item_widget)


def build_main_map_view(self):
    #TODO: Add in python functionality to control Javascript functions (parse desired data and plot on map)

    print("============+ Setting Up Map View +===============")



    # Set-up main map view.
    map_view_html = os.path.abspath(os.path.join(
        os.path.dirname(__file__), "../../resources/quake_select.html"))
    map_view_js = os.path.abspath(os.path.join(
        os.path.dirname(__file__), "../../resources/quake_select.js"))

    # print(map_view_html)

    self.ui.map_view_webEngine.load(QtCore.QUrl.fromLocalFile(map_view_html))
    self.ui.map_view_webEngine.loadFinished.connect(partial(onMapLoadFinished,
                                                            map=self.ui.map_view_webEngine,
                                                            js=map_view_js))


def build_dataview_map_view(self):
    print("============+ Setting Up Data Map View +===============")
    # set up the data-view small map
    dv_map_view_html = os.path.abspath(os.path.join(
        os.path.dirname(__file__), "../../resources/dv_map_view.html"))
    dv_map_view_js = os.path.abspath(os.path.join(
        os.path.dirname(__file__), "../../resources/dv_map_view.js"))

    self.ui.dv_map_view_webEngine.load(QtCore.QUrl.fromLocalFile(dv_map_view_html))
    self.ui.dv_map_view_webEngine.loadFinished.connect(partial(onMapLoadFinished,
                                                               map=self.ui.dv_map_view_webEngine,
                                                               js=dv_map_view_js))

def onMapLoadFinished(map, js):
    with open(js, 'r') as f:
        frame = map.page()