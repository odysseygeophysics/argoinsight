# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'waveform_plot_form.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_waveform_plot_Form(object):
    def setupUi(self, waveform_plot_Form):
        waveform_plot_Form.setObjectName("waveform_plot_Form")
        waveform_plot_Form.resize(798, 767)
        self.verticalLayout = QtWidgets.QVBoxLayout(waveform_plot_Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.waveform_stackedWidget = QtWidgets.QStackedWidget(waveform_plot_Form)
        self.waveform_stackedWidget.setObjectName("waveform_stackedWidget")
        self.page = QtWidgets.QWidget()
        self.page.setObjectName("page")
        self.waveform_stackedWidget.addWidget(self.page)
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setObjectName("page_2")
        self.waveform_stackedWidget.addWidget(self.page_2)
        self.horizontalLayout.addWidget(self.waveform_stackedWidget)
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.filter_checkBox = QtWidgets.QCheckBox(waveform_plot_Form)
        self.filter_checkBox.setText("")
        self.filter_checkBox.setObjectName("filter_checkBox")
        self.verticalLayout_5.addWidget(self.filter_checkBox)
        self.normalise_checkBox = QtWidgets.QCheckBox(waveform_plot_Form)
        self.normalise_checkBox.setText("")
        self.normalise_checkBox.setObjectName("normalise_checkBox")
        self.verticalLayout_5.addWidget(self.normalise_checkBox)
        self.mini_detrend_demean_checkBox = QtWidgets.QCheckBox(waveform_plot_Form)
        self.mini_detrend_demean_checkBox.setText("")
        self.mini_detrend_demean_checkBox.setObjectName("mini_detrend_demean_checkBox")
        self.verticalLayout_5.addWidget(self.mini_detrend_demean_checkBox)
        self.verticalLayout_6.addLayout(self.verticalLayout_5)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_6.addItem(spacerItem)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.custom_filter_toolButton = QtWidgets.QToolButton(waveform_plot_Form)
        self.custom_filter_toolButton.setObjectName("custom_filter_toolButton")
        self.verticalLayout_4.addWidget(self.custom_filter_toolButton)
        self.temp_toolButton_2 = QtWidgets.QToolButton(waveform_plot_Form)
        self.temp_toolButton_2.setObjectName("temp_toolButton_2")
        self.verticalLayout_4.addWidget(self.temp_toolButton_2)
        self.export_toolButton = QtWidgets.QToolButton(waveform_plot_Form)
        self.export_toolButton.setObjectName("export_toolButton")
        self.verticalLayout_4.addWidget(self.export_toolButton)
        self.verticalLayout_6.addLayout(self.verticalLayout_4)
        self.horizontalLayout.addLayout(self.verticalLayout_6)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(waveform_plot_Form)
        QtCore.QMetaObject.connectSlotsByName(waveform_plot_Form)

    def retranslateUi(self, waveform_plot_Form):
        _translate = QtCore.QCoreApplication.translate
        waveform_plot_Form.setWindowTitle(_translate("waveform_plot_Form", "Form"))
        self.custom_filter_toolButton.setText(_translate("waveform_plot_Form", "..."))
        self.temp_toolButton_2.setText(_translate("waveform_plot_Form", "..."))
        self.export_toolButton.setText(_translate("waveform_plot_Form", "..."))

