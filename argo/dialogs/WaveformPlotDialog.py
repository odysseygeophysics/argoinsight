import sys
from PyQt5 import QtGui, QtWidgets, QtCore
from obspy import read

import qdarkstyle
from argo.widgets.WaveformPlotWidget import WaveformPlotForm
from argo.widgets.WaveformListWidget import WaveformListWidget

class WaveformPlotDialog(QtWidgets.QDialog):


    def __init__(self, parent=None, st=None):
        super(WaveformPlotDialog, self).__init__(parent=parent)

        self.st = st

        self.unique_net_sta = set()

        for tr in self.st:
            tr_id = tr.get_id()
            elems = tr_id.split(".")
            self.unique_net_sta.add(elems[0]+"."+elems[1])

        # print(self.unique_net_sta)


        # add in listwidget that will contain individual plotviews of components for each station
        self.waveform_list_widget = WaveformListWidget()
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.waveform_list_widget)
        self.setLayout(layout)


        # add plot widget for each station to listwidget

        for net_sta in self.unique_net_sta:
            # print("Working on: ", net_sta)

            # get stream with traces just for this net/sta
            sel_st = self.st.select(network=net_sta.split(".")[0], station=net_sta.split(".")[1])

            # print(sel_st)

            # set up widget for net/sta
            waveform_item = QtWidgets.QListWidgetItem(self.waveform_list_widget)

            self.waveform_item_widget = WaveformPlotForm()

            self.waveform_item_widget.plotWaveformData(sel_st)

            waveform_item.setSizeHint(self.waveform_item_widget.sizeHint())
            self.waveform_list_widget.addItem(waveform_item)

            self.waveform_list_widget.setItemWidget(waveform_item, self.waveform_item_widget)

        self.show()
        self.raise_()





def main():
    st_test = read("../../tests/test_resources/stream_test.mseed")
    # print(st_test)


    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    dlg = WaveformPlotDialog(None, st_test)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()