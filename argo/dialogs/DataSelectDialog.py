import sys
import qdarkstyle

import pandas as pd
from enum import Enum

from PyQt5 import QtGui, QtWidgets, QtCore
from argo.ui.data_select_window import Ui_data_select_Dialog

from argo.widgets.DataListView import DataListView



class DataSelectTypes(Enum):
    Network= 0
    Station= 1
    Channel= 2
    Location= 3


#TODO: Make the opertaion faster
#TODO: Make it so the window never closes, once initialised. Just minimizes

class DataSelectDialog(QtWidgets.QDialog):
    dataSelectionChanged = QtCore.pyqtSignal(object, object)


    def __init__(self, parent=None, df=None):
        super(DataSelectDialog, self).__init__(parent=parent)
        self.dataselui = Ui_data_select_Dialog()
        self.dataselui.setupUi(self)

        # set up data selected dictionary for levels
        self.data_sel_dict = {}

        # add in the custom listviews
        # add in the holdings Table view
        self.dataselui.net_listView = DataListView(list_type="Network")
        self.dataselui.net_stackedWidget.addWidget(self.dataselui.net_listView)
        self.dataselui.net_stackedWidget.setCurrentWidget(self.dataselui.net_listView)
        #connect signal and slot in listview
        self.connectDataListView(self.dataselui.net_listView)

        self.dataselui.sta_listView = DataListView(list_type="Station")
        self.dataselui.sta_stackedWidget.addWidget(self.dataselui.sta_listView)
        self.dataselui.sta_stackedWidget.setCurrentWidget(self.dataselui.sta_listView)
        # connect signal and slot in listview
        self.connectDataListView(self.dataselui.sta_listView)

        self.dataselui.chan_listView = DataListView(list_type="Channel")
        self.dataselui.chan_stackedWidget.addWidget(self.dataselui.chan_listView)
        self.dataselui.chan_stackedWidget.setCurrentWidget(self.dataselui.chan_listView)
        # connect signal and slot in listview
        self.connectDataListView(self.dataselui.chan_listView)

        self.dataselui.loc_listView = DataListView(list_type="Location")
        self.dataselui.loc_stackedWidget.addWidget(self.dataselui.loc_listView)
        self.dataselui.loc_stackedWidget.setCurrentWidget(self.dataselui.loc_listView)
        # connect signal and slot in listview
        self.connectDataListView(self.dataselui.loc_listView)

        self.dataselui.tags_listView = DataListView(list_type="Tag")
        self.dataselui.tags_stackedWidget.addWidget(self.dataselui.tags_listView)
        self.dataselui.tags_stackedWidget.setCurrentWidget(self.dataselui.tags_listView)
        # connect signal and slot in listview
        self.connectDataListView(self.dataselui.tags_listView)

        self.data_df = df

        self.build_unique_data_df()

        self.buildDataSelectTrees()

        # when you want to destroy the dialog set this to True
        self._want_to_close = False

        self.show()
        self.raise_()

    # def closeEvent(self, event):
    #     if self._want_to_close:
    #         super(DataSelectDialog, self).closeEvent(event)
    #     else:
    #         event.ignore()
    #         self.setVisible(False)
    #         # self.setWindowState(QtCore.Qt.WindowMinimized)

    # method to run on data selection changed signal
    @QtCore.pyqtSlot(int, str, str)
    def get_selected_nscl_data(self, df_index, list_type, sel_bool):
        # print(df_index, list_type, sel_bool)
        #Update the df with checked items
        self.unique_data_df_dict[list_type].at[df_index, "Checked"] = sel_bool
        self.buildDataSelectTrees(list_type)

        # only return the lowest level df_dict in data_sel_dict
        keys_list = self.data_sel_dict.keys()
        keys_val_list = [DataSelectTypes[x].value for x in keys_list]
        # get max level
        max_level_key = DataSelectTypes(max(keys_val_list)).name

        # print(max_level_key)

        self.dataSelectionChanged.emit(self.unique_data_df_dict, self.data_sel_dict[max_level_key])

    def connectDataListView(self, listView):
        listView.listItemChecked.connect(self.get_selected_nscl_data)

    def build_unique_data_df(self):
        """
        method to build a df of unique codes for each of Network, Station, Channel, Location, ASDF Tags
        with a column showing visibility selection in tree view
        """

        # dict to contain DF's
        self.unique_data_df_dict = {}

        for column in self.data_df:
            # print(column, self.data_df[column])

            # create new df for each header:
            unique_data_df = pd.DataFrame(data=None, columns=["Codes", "Vis", "Checked"])

            for _i, code in enumerate(self.data_df[column]):
                # print(_i, code)

                if column == "Network":
                    # set all of the items to checkable
                    unique_data_df.loc[_i] = [code, "True", "False"]
                else:
                    # set all of the items to non - checkable for now
                    unique_data_df.loc[_i] = [code, "False", "False"]



            unique_data_df.drop_duplicates(inplace=True)
            unique_data_df.reset_index(drop=True, inplace=True)
            # print(unique_data_df)
            # add the df to dict
            self.unique_data_df_dict[column] = unique_data_df

    def buildDataSelectTrees(self, list_type=None):
        """
        Method for building list views for data (Seprate List view for Network, Station, Channel, Location, ASDF Tags
        """

        # -------- add networks to network select items
        self.dataselui.net_listView.updateDataList(self.unique_data_df_dict["Network"])


        # -------- add stations to staion list view
        #first check visibility
        # print("==============")
        self.check_visibility("Station")

        self.dataselui.sta_listView.updateDataList(self.unique_data_df_dict["Station"])

        # -------- add channels to channel list view
        # first check visibility
        # print("==============")
        self.check_visibility("Channel")

        self.dataselui.chan_listView.updateDataList(self.unique_data_df_dict["Channel"])

        # -------- add locations to location list view
        # first check visibility
        # print("==============")
        self.check_visibility("Location")

        self.dataselui.loc_listView.updateDataList(self.unique_data_df_dict["Location"])

    def check_visibility(self, list_type):
        # for index, row in self.unique_data_df_dict[list_type].iterrows():
        #     self.code_valid_lookup(row["Codes"], list_type)

        self.code_valid_lookup(self.unique_data_df_dict[list_type]["Codes"].tolist(), list_type)

    def code_valid_lookup(self, code_list, level):
        """
        Method to look up original df to see if a particular code (NSCL) at whatever level
        is valid for selection based on the parent selected codes
        """

        # print("=========================")
        # print("Level: ", level)
        # print("Looking at:", code_list)

        lookup_df = self.data_df.copy()

        def split_column_levels(level):
            drp_col_list = []
            active_col_list = []
            for _i, data_type in enumerate(DataSelectTypes):
                if _i > DataSelectTypes[level].value:
                    drp_col_list.append(data_type.name)
                else:
                    active_col_list.append(data_type.name)
            return (active_col_list, drp_col_list)

        active_cols, drp_cols = split_column_levels(level)

        # print(active_cols)
        # print(drp_cols)

        lookup_values = {level: code_list}

        # get checked codes for active columns
        for active_type in active_cols:
            if not active_type == level:
                # print("looking for checked parents", active_type)
                lev_df = self.unique_data_df_dict[active_type]

                checked_parents_df = lev_df[lev_df["Checked"] == "True"]

                parent_codes_list = []

                # append the codes of checked parents to value_comparison dict
                for checked_code in checked_parents_df["Codes"]:
                    parent_codes_list.append(checked_code)

                lookup_values[active_type] = parent_codes_list

        # print("Using: ")
        # print(lookup_values)

        # drop list types that are below the investigated level
        lookup_df.drop(drp_cols, axis=1, inplace=True)

        # print(lookup_df)

        row_mask = lookup_df.isin(lookup_values).all(1)

        # print(lookup_df.isin(lookup_values))

        # retreive the full entry in original df that match the criteria for given level
        full_data_array = self.data_df[row_mask]

        if not full_data_array.empty:
            self.data_sel_dict[level] = full_data_array

        unique_df_mask = self.unique_data_df_dict[level].isin({"Codes": lookup_df[row_mask][level].tolist()})

        print(unique_df_mask)

        for index, row in unique_df_mask.iterrows():

            if row["Codes"] == True:
                self.unique_data_df_dict[level].at[index, "Vis"] = "True"
            else:
                self.unique_data_df_dict[level].at[index, "Vis"] = "False"

        print(self.unique_data_df_dict[level])




def main():
    d = {'Network': ["OA", "OA", "OA", "OA", "AU", "DT"],
         'Station': ["CC25", "CC26", "CB25", "CB25", "MUXOZ", "OBS02"],
         "Location": ["0L", "0L", "0N", "0N", "", ""],
         "Channel": ["BIO", "BYV", "HHE", "HHZ", "HHZ", "BHZ"]}

    df = pd.DataFrame(data=d)


    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    dlg = DataSelectDialog(None, df)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
