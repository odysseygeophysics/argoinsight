import sys
import qdarkstyle
import pyqtgraph as pg
import numpy as np
import os


from functools import partial
from obspy import UTCDateTime

import pyasdf
from PyQt5 import QtGui, QtWidgets, QtCore
from argo.ui.timing_qc_window import Ui_TimingQCWindow
from argo.database.seisds import SeisDB
from pyqtgraph.graphicsItems.GradientEditorItem import Gradients
from matplotlib import cm, colors

from pyqtgraph import AxisItem, ErrorBarItem, ColorMap, LinearRegionItem

from argo.utils.General import cmapToColormap
from argo.utils.DrawGraphItem import DrawGraphItem
import pandas as pd


class TimingQCWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None, ds=None, db=None):
        super(TimingQCWindow, self).__init__(parent=parent)
        self.timingQCui = Ui_TimingQCWindow()
        self.timingQCui.setupUi(self)


        # self.timingQCui.cc_graph_widget = pg.ImageView()
        self.cc_graph_layout_widget = pg.GraphicsLayoutWidget()
        self.timingQCui.cc_graph_stackedWidget.addWidget(self.cc_graph_layout_widget)
        self.timingQCui.cc_graph_stackedWidget.setCurrentWidget(self.cc_graph_layout_widget)

        # self.timingQCui.cc_graph_widget = pg.ImageView()
        self.timeshift_graph_layout_widget = pg.GraphicsLayoutWidget()
        self.timingQCui.timeshift_graph_stackedWidget.addWidget(self.timeshift_graph_layout_widget)
        self.timingQCui.timeshift_graph_stackedWidget.setCurrentWidget(self.timeshift_graph_layout_widget)

        self.ds = ds
        self.db = db

        self.timingQCui.analyse_net_sta_pushButton.released.connect(self.analyse_net_sta)

        # connect signals for combo boxes changed:
        self.timingQCui.control_comboBox.activated.connect(self.control_combo_box_activated)
        self.timingQCui.ref_comboBox.activated.connect(self.ref_combo_box_activated)

        self.selected_net_sta_pair = {"temp_net_sta": None, "ref_net_sta": None}


        self.build_station_view()
        self.build_map_view()

        self.show()
        self.raise_()


    def analyse_net_sta(self):
        print(self.selected_net_sta_pair)
        if not None in self.selected_net_sta_pair.values():
            print("Extracting XCOR data and plotting")
            self.get_selected_data(temp_net_sta=self.selected_net_sta_pair["temp_net_sta"], ref_net_sta=self.selected_net_sta_pair["ref_net_sta"])
            print("Plotting XCOR")
            self.update_xcor_plot()

    def control_combo_box_activated(self,index):
        # will only trigger when combo box is changed via user (not programatically)
        #change the selected station in list view
        self.timingQCui.temp_sta_listWidget.setCurrentItem(self.timingQCui.temp_sta_listWidget.item(index))
        self.selected_net_sta_pair["temp_net_sta"] = self.temp_sta_unique_series[index]

    def ref_combo_box_activated(self,index):
        # will only trigger when combo box is changed via user (not programatically)
        # change the selected station in list view
        self.timingQCui.ref_sta_listWidget.setCurrentItem(self.timingQCui.ref_sta_listWidget.item(index))
        self.selected_net_sta_pair["ref_net_sta"] = self.ref_sta_unique_series[index]


    def temp_sta_list_item_clicked(self, item):
        # print(item.text())
        net_sta = item.text()

        # get index of net_sta
        sel_index = self.temp_sta_unique_series[self.temp_sta_unique_series == net_sta].index.item()

        self.selected_net_sta_pair["temp_net_sta"] = net_sta
        # TODO: Highlight selected station on map view
        # modify combo box
        self.timingQCui.control_comboBox.setCurrentIndex(sel_index)

    def ref_sta_list_item_clicked(self, item):
        # print(item.text())
        net_sta = item.text()
        # get index of net_sta
        sel_index = self.ref_sta_unique_series[self.ref_sta_unique_series == net_sta].index.item()
        self.selected_net_sta_pair["ref_net_sta"] = net_sta
        # TODO: Highlight selected station on map view
        # modify combo box
        self.timingQCui.ref_comboBox.setCurrentIndex(sel_index)

    def onMapLoadFinished(self, map, js):
        with open(js, 'r') as f:
            frame = map.page()

    def build_map_view(self):
        # TODO: Add in python functionality to control Javascript functions (parse desired data and plot on map)
 
        # Set-up main map view.
        map_view_html = os.path.abspath(os.path.join(
            os.path.dirname(__file__), "../../resources/quake_select.html"))
        map_view_js = os.path.abspath(os.path.join(
            os.path.dirname(__file__), "../../resources/quake_select.js"))

        # print(map_view_html)

        self.timingQCui.cc_map_view_webEngine.load(QtCore.QUrl.fromLocalFile(map_view_html))
        self.timingQCui.cc_map_view_webEngine.loadFinished.connect(partial(self.onMapLoadFinished,
                                                                map=self.timingQCui.cc_map_view_webEngine,
                                                                js=map_view_js))

    def build_station_view(self):

        self.net_sta_lookup_df = pd.DataFrame(data=None, columns=["temp_sta", "ref_sta"])

        # create pandas dataframe with net_sta pairs
        for _i, net_sta_pair in enumerate(self.ds.auxiliary_data.XCOR.list()):
            self.net_sta_lookup_df.loc[_i] = [net_sta_pair.split("__")[0], net_sta_pair.split("__")[1]]

        self.temp_sta_unique_series = self.net_sta_lookup_df["temp_sta"].drop_duplicates()
        self.ref_sta_unique_series = self.net_sta_lookup_df["ref_sta"].drop_duplicates()

        self.temp_sta_unique_series.reset_index(inplace=True, drop=True)
        self.temp_sta_unique_series.reset_index(inplace=True, drop=True)

        # get the unique temp net_sta in data
        for net_sta in self.temp_sta_unique_series:
            self.timingQCui.temp_sta_listWidget.addItem(net_sta)
            # add the station to the combo box
            self.timingQCui.control_comboBox.addItem(net_sta)


        # get the unique ref net_sta in data
        for net_sta in self.ref_sta_unique_series:
            self.timingQCui.ref_sta_listWidget.addItem(net_sta)
            # add the station to the combo box
            self.timingQCui.ref_comboBox.addItem(net_sta)

        self.timingQCui.temp_sta_listWidget.itemClicked.connect(self.temp_sta_list_item_clicked)
        self.timingQCui.ref_sta_listWidget.itemClicked.connect(self.ref_sta_list_item_clicked)

    def dispMousePos(self, pos):
        if self.img_plot.sceneBoundingRect().contains(pos):
            # Display current mouse coords if over the image plot area as a tooltip
            x_coord = self.img_plot.vb.mapSceneToView(pos).toPoint().x()
            y_coord = self.img_plot.vb.mapSceneToView(pos).toPoint().y()

            time_tool = self.img_plot.setToolTip(str(x_coord))

    def cc_graph_widget_clicked(self, event):
        # TODO: Display other information about XCOR (no windows, Timeframe etc) in textItem on mouse click
        # TODO: somehow highlight the clicked interval on the xcor image
        # TODO: add in right click functionality to retrieve xcor for just the single interval and plot also have option to look at timeseries data for that region
        pos = QtCore.QPointF(event.scenePos())
        if self.img_plot.sceneBoundingRect().contains(pos):
            vb = self.img_plot.vb
            x_coord = vb.mapSceneToView(pos).toPoint().x()
            y_coord = vb.mapSceneToView(pos).toPoint().y()

            shift_query = event.modifiers() == QtCore.Qt.ShiftModifier
            scroll_btn_query = event.button() == 4

            # # use the y coord to find the associated parameters data:
            # int_starttime = self.parameters_dict["starttime"][y_coord]
            # int_endtime = self.parameters_dict["endtime"][y_coord]
            # no_stacked_windows = self.parameters_dict["no_stacked_windows"][y_coord]

            display_text_item = pg.TextItem(
                html='<div style="text-align: center"><span style="color: #FFF;">This is the</span><br><span style="color: #FF0; font-size: 16pt;">PEAK</span></div>',
                anchor=(-0.3, 0.5), angle=45, border='w', fill=(0, 0, 255, 100))

            self.img_plot.addItem(display_text_item)
            display_text_item.setPos(x_coord, y_coord)

            # time_tool = self.img_plot.setToolTip(str(int_starttime) +"_"+ str(int_endtime))



    def update_xcor_plot(self):

        def build_axis_item(start, stop, axis_len, axis_scale, orientation, levels):
            # modify values in levels with scaling factor
            scaled_levels = [int(x * axis_scale) for x in list(levels)]

            # create numpy array from desired start value with a axis value for every data point
            x_axis_val_array = np.linspace(0, axis_len * axis_scale, axis_len, endpoint=False)
            x_axis_string_array = np.linspace(start, stop, axis_len, endpoint=False)

            axis_ticks = []

            for level in scaled_levels:
                # create level list of val, string tuples
                level_list = list(zip(x_axis_val_array[::level], x_axis_string_array[::level]))
                axis_ticks.append(level_list)

            axis_item = AxisItem(orientation=orientation)
            axis_item.setTicks(axis_ticks)
            return axis_item

        x_axis_scale = 1
        y_axis_scale = 1

        # Contrast/color control
        hist = pg.HistogramLUTItem()
        # Item for displaying image data
        self.img = pg.ImageItem()

        hist.setImageItem(self.img)
        hist.vb.setMouseEnabled(y=False)

        # add items to plot area
        self.cc_graph_layout_widget.addItem(hist)




        # set up plot area for displaying data:
        self.img_plot = self.cc_graph_layout_widget.addPlot(
            axisItems={
                'bottom': build_axis_item(levels=(10000, 1000, 100),
                                          start=-3600,
                                          stop=3600,
                                          axis_scale=x_axis_scale,
                                          axis_len=self.xcorr_array.shape[1],
                                          orientation="bottom"),
                'left': build_axis_item(levels=(10, 1),
                                        start=0,
                                        stop=self.xcorr_array.shape[0],
                                        axis_scale=y_axis_scale,
                                        axis_len=self.xcorr_array.shape[0],
                                        orientation="left")
            })

        self.img_plot.addItem(self.img)

        cmap = cmapToColormap(cm.get_cmap('jet'))

        # # Contrast/color control
        # hist = pg.HistogramLUTItem()
        # hist.setImageItem(self.img)
        # self.cc_graph_layout_widget.addItem(hist)
        # hist.vb.setMouseEnabled(y=False)

        # set image data
        self.img.setImage(self.xcorr_array.T, autoRange=True)
        self.img.scale(x_axis_scale, y_axis_scale)

        hist.setLevels(self.xcorr_array.min(), self.xcorr_array.max())
        hist.gradient.setColorMap(cmap)

        self.cc_graph_layout_widget.scene().sigMouseMoved.connect(self.dispMousePos)
        # self.timingQCui.timingQCui.cc_graph_layout_widget.scene().sigMouseClicked.connect(self.cc_graph_widget_clicked)


        #------------- Add in plots for no windows, person coef and est timeshift ----------------#

        # set up plot area for displaying data:
        self.no__stacked_windows_plot = self.cc_graph_layout_widget.addPlot()

        self.pearson_coeff_plot = self.cc_graph_layout_widget.addPlot()

        self.calculated_timeshift_plot = self.cc_graph_layout_widget.addPlot(
            axisItems={
                'right': build_axis_item(levels=(10, 1),
                                        start=0,
                                        stop=self.xcorr_array.shape[0],
                                        axis_scale=y_axis_scale,
                                        axis_len=self.xcorr_array.shape[0],
                                        orientation="right")
            })


        self.no__stacked_windows_plot.hideAxis("left")
        self.pearson_coeff_plot.hideAxis("left")
        self.calculated_timeshift_plot.hideAxis("left")

        self.calculated_timeshift_plot.showAxis("right")

        self.no__stacked_windows_plot.setYLink(self.img_plot)
        self.pearson_coeff_plot.setYLink(self.img_plot)
        self.calculated_timeshift_plot.setYLink(self.img_plot)

        self.no__stacked_windows_plot.plot(self.parameters_dict["no_stacked_windows"], self.interval_array)
        self.pearson_coeff_plot.plot(self.pearson_array, self.interval_array)
        self.calculated_timeshift_plot.plot(self.calculated_timeshift_array, self.interval_array)

        self.cc_graph_layout_widget.ci.layout.setColumnMaximumWidth(0, 50.0)
        self.cc_graph_layout_widget.ci.layout.setColumnMaximumWidth(2, 100.0)
        self.cc_graph_layout_widget.ci.layout.setColumnMaximumWidth(3, 100.0)
        self.cc_graph_layout_widget.ci.layout.setColumnMaximumWidth(4, 100.0)

        # ------------- plot calculated timeshift in second dialog box with user capability to digitise  ----------------#

        self.timeshift_edit_plot = self.timeshift_graph_layout_widget.addPlot(
            axisItems={
                'left': build_axis_item(levels=(10, 1),
                                        start=0,
                                        stop=self.xcorr_array.shape[0],
                                        axis_scale=y_axis_scale,
                                        axis_len=self.xcorr_array.shape[0],
                                        orientation="left")
            })

        self.timeshift_edit_plot.plot(self.calculated_timeshift_array, self.interval_array)
        # self.timeshift_edit_plot.setYLink(self.img_plot)
        #TODO: unlink y axis when dialog is moved to window


        self.draw_plot = DrawGraphItem()
        self.timeshift_edit_plot.addItem(self.draw_plot)

        pos = np.column_stack((self.calculated_timeshift_array, self.interval_array))
        adj = np.column_stack((np.linspace(0, self.interval_array.shape[0]-1, num=self.interval_array.shape[0]-1, endpoint=False, dtype=int), np.linspace(1, self.interval_array.shape[0]-1, num=self.interval_array.shape[0]-1, endpoint=True, dtype=int)))

        ## Define text to show next to each symbol
        texts = ["Point %d" % i for i in range(self.interval_array.shape[0])]

        ## Update the graph
        self.draw_plot.setData(pos=pos, adj=adj, size=1, pxMode=False)




    def get_selected_data(self, temp_net_sta, ref_net_sta):

        # xcor array size(x,y) where x is intervals and y is xcorr (14599999)
        temp_array = self.ds.auxiliary_data.XCOR[temp_net_sta.replace(".", "_") + "__"+ref_net_sta.replace(".", "_")].data[:]
        # downsample the x(lag) axis by 10
        self.xcorr_array = temp_array[:,::10]
        self.parameters_dict = self.ds.auxiliary_data.XCOR[temp_net_sta.replace(".", "_") + "__"+ref_net_sta.replace(".", "_")].parameters


        self.interval_array = np.array(list(range(self.xcorr_array.shape[0])))


        #TODO: calculate person coefficient and estimated time shift
        self.pearson_array = np.random.random_sample(size=(self.xcorr_array.shape[0]))
        self.calculated_timeshift_array = np.random.random_sample(size=(self.xcorr_array.shape[0],))*10

        print(self.parameters_dict["no_stacked_windows"])






def main():
    testing_ds = pyasdf.ASDFDataSet("/Users/ashbycooper/Desktop/mock_AWS/Processing/OA_xcor.h5")
    testing_db = SeisDB(json_file="/Users/ashbycooper/Desktop/mock_AWS/Processing/OA_xcor_raw_dataDB.json")

    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    window = TimingQCWindow(None, ds=testing_ds, db=testing_db)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
