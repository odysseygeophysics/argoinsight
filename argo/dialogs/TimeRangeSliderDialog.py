import sys
from PyQt5 import QtGui, QtWidgets, QtCore
from obspy import read
import numpy as np
from obspy import UTCDateTime

import qdarkstyle
from argo.widgets.TimeRangeSliderWidget import TimeRangeSliderForm

class TimeRangeSliderDialog(QtWidgets.QDialog):


    def __init__(self, parent=None, rec_intervals_array=None, st=None, et=None):
        super(TimeRangeSliderDialog, self).__init__(parent=parent)

        self.time_range_slider_widget = TimeRangeSliderForm()
        self.time_range_slider_widget.update_time_slider_data(rec_intervals_array=rec_intervals_array, st=st, et=et)
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.time_range_slider_widget)
        self.setLayout(layout)


        self.show()
        self.raise_()





def main():
    rec_start_list = [UTCDateTime("2018-01-01T12:00:00").timestamp, UTCDateTime("2018-02-05T12:00:00").timestamp, UTCDateTime("2018-02-12T12:00:00").timestamp]
    rec_end_list = [UTCDateTime("2018-02-01T12:00:00").timestamp, UTCDateTime("2018-02-10T12:00:00").timestamp, UTCDateTime("2018-03-12T12:00:00").timestamp]
    rec_intervals_array = np.array([rec_start_list, rec_end_list])

    earliest = rec_intervals_array[0][0]
    latest = rec_intervals_array[1][-1]

    total_time = latest - earliest

    frac_time = total_time * 0.1


    # print(rec_intervals_array)


    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    dlg = TimeRangeSliderDialog(None, rec_intervals_array=rec_intervals_array, st=earliest-frac_time, et=latest+frac_time)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()