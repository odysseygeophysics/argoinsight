
from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore

class WaveformListWidget(QtWidgets.QListWidget):

    def __init__(self, *args, **kwargs):
        QtWidgets.QListWidget.__init__(self, *args, **kwargs)

    # ignore the mouse scroll wheel to stop interfering with waveform graph controls
    def wheelEvent(self, ev: QtGui.QWheelEvent):
        if ev.type() == QtCore.QEvent.Wheel:
            ev.ignore()
