from argo.ui.time_range_slider_form import Ui_time_range_slider
from PyQt5 import QtCore, QtWidgets
from pyqtgraph.widgets.GraphicsLayoutWidget import GraphicsLayoutWidget
from argo.utils.DateAxisItem import DateAxisItem
from obspy import UTCDateTime

from pyqtgraph import ErrorBarItem, AxisItem, LinearRegionItem
import numpy as np


class TimeRangeSliderForm(QtWidgets.QWidget):
    # Signals
    RangeValuesChanged = QtCore.pyqtSignal(int, int)
    GoWaveformPlot = QtCore.pyqtSignal(bool)


    def __init__(self, parent=None):
        super(TimeRangeSliderForm, self).__init__(parent=parent)
        self.timesliderui = Ui_time_range_slider()
        self.timesliderui.setupUi(self)

        # add in the time slider graphics view to the stacked widget (Pyqtgraph graphics view)
        self.timesliderui.time_slider_graph_view = GraphicsLayoutWidget()
        self.timesliderui.time_slider_stackedWidget.addWidget(self.timesliderui.time_slider_graph_view)
        self.timesliderui.time_slider_stackedWidget.setCurrentWidget(self.timesliderui.time_slider_graph_view)

        # install event filter to capture currently focussed widget (enable us to work out which widget has made a change)
        self.timesliderui.starttime_dateTimeEdit.installEventFilter(self)
        self.timesliderui.endtime_dateTimeEdit.installEventFilter(self)
        self.timesliderui.time_slider_graph_view.installEventFilter(self)

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.FocusIn:
            if obj == self.timesliderui.starttime_dateTimeEdit:
                # print("Starttime DateTime Edit")
                self.active_widget = self.timesliderui.starttime_dateTimeEdit
            elif obj == self.timesliderui.endtime_dateTimeEdit:
                # print("Endtime DateTime Edit")
                self.active_widget = self.timesliderui.endtime_dateTimeEdit
            elif obj == self.timesliderui.time_slider_graph_view:
                # print("Graph View")
                self.active_widget = self.timesliderui.time_slider_graph_view
            else: self.active_widget = "main"
        return super(TimeRangeSliderForm, self).eventFilter(obj, event)


    def on_go_waveform_toolButton_released(self):
        # print("Button Released")
        self.GoWaveformPlot.emit(self.timesliderui.go_waveform_toolButton.isChecked())

    def update_time_slider_data(self, rec_intervals_array, st, et):

        print("Updating Rec Intervals")
        print(rec_intervals_array)
        # update the recording intervals array for plotting on time range slider plot
        self.rec_intervals_array = rec_intervals_array
        self.initial_st = st
        self.initial_et = et

        total_time = self.initial_et - self.initial_st
        # time in seconds that is 10 percent of the region covered by the earliest and latest times from recording intervals
        self.frac_time = total_time * 0.1

        self.update_rec_int_plot()
        self.build_time_slider_roi()
        self.set_spin_box_initial()

    def set_spin_box_initial(self):

        # set the min and max and displayed time on datetime spin box
        self.timesliderui.starttime_dateTimeEdit.setMinimumDateTime(
            QtCore.QDateTime.fromString(str(UTCDateTime(self.initial_st)).split(".")[0], "yyyy-MM-ddThh:mm:ss"))
        self.timesliderui.starttime_dateTimeEdit.setMaximumDateTime(
            QtCore.QDateTime.fromString(str(UTCDateTime(self.initial_et)).split(".")[0], "yyyy-MM-ddThh:mm:ss"))
        self.timesliderui.endtime_dateTimeEdit.setMinimumDateTime(
            QtCore.QDateTime.fromString(str(UTCDateTime(self.initial_st)).split(".")[0], "yyyy-MM-ddThh:mm:ss"))
        self.timesliderui.endtime_dateTimeEdit.setMaximumDateTime(
            QtCore.QDateTime.fromString(str(UTCDateTime(self.initial_et)).split(".")[0], "yyyy-MM-ddThh:mm:ss"))


        # self.set_spin_box_dynamic(self.initial_st+self.frac_time, self.initial_et-self.frac_time)

    def set_spin_box_dynamic(self, start, end):
        # print(self.active_widget)

        self.timesliderui.starttime_dateTimeEdit.setDateTime(
            QtCore.QDateTime.fromString(str(UTCDateTime(start)).split(".")[0], "yyyy-MM-ddThh:mm:ss"))
        self.timesliderui.endtime_dateTimeEdit.setDateTime(
            QtCore.QDateTime.fromString(str(UTCDateTime(end)).split(".")[0], "yyyy-MM-ddThh:mm:ss"))

    def on_starttime_dateTimeEdit_dateTimeChanged(self):
        # date time spin box changed. Change the ROI box and emit signal if it was the date tiem edit box that made the change
        if self.active_widget in [self.timesliderui.starttime_dateTimeEdit]:#, "initial"]:
            self.set_time_slider_region(
                UTCDateTime(self.timesliderui.starttime_dateTimeEdit.dateTime().toPyDateTime()).timestamp,
                UTCDateTime(self.timesliderui.endtime_dateTimeEdit.dateTime().toPyDateTime()).timestamp)

    def on_endtime_dateTimeEdit_dateTimeChanged(self):
        # date time spin box changed. Chnage the ROI box and emit signal if it was the date tiem edit box that made the change
        if self.active_widget in [self.timesliderui.endtime_dateTimeEdit]:#, "initial"]:
            self.set_time_slider_region(UTCDateTime(self.timesliderui.starttime_dateTimeEdit.dateTime().toPyDateTime()).timestamp,
                                        UTCDateTime(
                self.timesliderui.endtime_dateTimeEdit.dateTime().toPyDateTime()).timestamp)

    def get_roi_data(self):
        current_region = self.lri.getRegion()
        self.RangeValuesChanged.emit(current_region[0], current_region[1])
        if not self.active_widget in [self.timesliderui.starttime_dateTimeEdit, self.timesliderui.endtime_dateTimeEdit]:
            self.set_spin_box_dynamic(current_region[0], current_region[1])

    def build_time_slider_roi(self):
        # set up ROI (region of interest) box
        self.lri = LinearRegionItem(
            values=[0, 1], movable=True)

        self.lri.setBounds([self.initial_st-self.frac_time, self.initial_et + self.frac_time])

        # set the focus widget to "initial" to start with
        self.active_widget = "initial"

        # connect to roi changed signal
        self.lri.sigRegionChangeFinished.connect(self.get_roi_data)

        self.time_slider_plot.addItem(self.lri)

    def set_time_slider_region(self, start, end):
        # change ROI with start and end values
        print("set_time_slider_region", start, end)
        self.lri.setRegion((start, end))

    def dispMousePos(self, pos):
        # Display current mouse coords if over the scatter plot area as a tooltip
        try:
            x_coord = UTCDateTime(self.time_slider_plot.vb.mapSceneToView(pos).toPoint().x()).ctime()
            time_tool = self.time_slider_plot.setToolTip(x_coord)
        except:
            pass

    def update_rec_int_plot(self):

        self.timesliderui.time_slider_graph_view.clear()

        # set up y axis values and labels
        y_labels_dict = {"Availability": 0}
        enum_y = list(enumerate(y_labels_dict.keys()))

        y_axis_string = AxisItem(orientation='left')
        y_axis_string.setTicks([enum_y])

        # Set up the plotting area
        self.time_slider_plot = self.timesliderui.time_slider_graph_view.addPlot(0, 0,
                                                                                 axisItems={'bottom': DateAxisItem(
                                                                                     orientation='bottom',
                                                                                     utcOffset=0),
                                                                                     'left': y_axis_string})
        self.time_slider_plot.setMouseEnabled(x=True, y=False)
        # When Mouse is moved over plot print the data coordinates
        self.time_slider_plot.scene().sigMouseMoved.connect(self.dispMousePos)

        # Re-establish previous view limits if it exists
        if hasattr(self, "saved_state"):
            self.time_slider_plot.getViewBox().setState(self.saved_state)

        y_axis_values = []
        rec_midpoints = []
        diff_frm_mid_list = []

        # iterate through recording array
        for _i in range(self.rec_intervals_array.shape[1]):
            diff_frm_mid = (self.rec_intervals_array[1, _i] - self.rec_intervals_array[0, _i]) / 2.0

            diff_frm_mid_list.append(diff_frm_mid)

            rec_midpoints.append(self.rec_intervals_array[0, _i] + diff_frm_mid)

            y_axis_values.append(0)

        # Plot Error bar data recording intervals
        err = ErrorBarItem(x=np.array(rec_midpoints), y=np.array(y_axis_values), left=np.array(diff_frm_mid_list),
                           right=np.array(diff_frm_mid_list), beam=0.06)

        err.setZValue(10)

        self.time_slider_plot.addItem(err)

        self.time_slider_plot.setXRange(self.initial_st - self.frac_time, self.initial_et + self.frac_time)
        self.time_slider_plot.setLimits(xMin=self.initial_st - self.frac_time, xMax=self.initial_et + self.frac_time)
