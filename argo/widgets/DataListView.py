
from PyQt5 import QtWidgets
from PyQt5 import QtCore, QtGui


class DataListView(QtWidgets.QListView):
    # signal for when list item is toggled (returns: index for df, type of list, bool (str) checked or not)
    listItemChecked = QtCore.pyqtSignal(int, str, str)

    def __init__(self, list_type=None, *args, **kwargs):
        QtWidgets.QListView.__init__(self, *args, **kwargs)
        # print(list_type)
        self.list_type = list_type

    def data_selection_changed(self, item):
        # print(item.checkState())
        # print(item.index())
        # print(item.index().row())
        # emit the listItemChecked signal
        if item.checkState() == 0:
            self.listItemChecked.emit(item.index().row(), self.list_type, "False")
        elif item.checkState() == 2:
            self.listItemChecked.emit(item.index().row(), self.list_type, "True")

    def updateDataList(self, list_df):
        # print(list_df)

        self.list_model = QtGui.QStandardItemModel(self)

        for index, row in list_df.iterrows():
            checked = row["Checked"]
            visibility = row["Vis"]
            item = QtGui.QStandardItem(row["Codes"])
            item.setCheckable(True)

            # check if the item should be checked or not
            if checked == "True":
                item.setCheckState(QtCore.Qt.Checked)
            elif checked == "False":
                item.setCheckState(QtCore.Qt.Unchecked)

            # check the item visibility (i.e. if it should be enabled or not)
            if visibility == "True":
                item.setEnabled(True)
            elif visibility == "False":
                item.setCheckState(QtCore.Qt.Unchecked)
                item.setEnabled(False)

            self.list_model.appendRow(item)

        self.list_model.itemChanged.connect(self.data_selection_changed)

        self.setModel(self.list_model)