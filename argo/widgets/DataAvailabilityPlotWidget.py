
from PyQt5 import QtCore, QtWidgets
from pyqtgraph import AxisItem, ErrorBarItem, ColorMap, LinearRegionItem
from pyqtgraph.widgets.GraphicsView import GraphicsView
from pyqtgraph.graphicsItems import MultiPlotItem as MultiPlotItem

from argo.utils.DateAxisItem import DateAxisItem
from argo.ui.data_avail_plot_form import Ui_data_avail_plot_Form

import numpy as np

from pyqtgraph.graphicsItems.GradientEditorItem import Gradients
from pyqtgraph.widgets.GraphicsLayoutWidget import GraphicsLayoutWidget

from obspy import UTCDateTime
from matplotlib import cm, colors




class DataAvailabilityPlotForm(QtWidgets.QWidget):
    # Signal for when range values are changed on Data Avaalibality plot
    DataAvailRangeValuesChanged = QtCore.pyqtSignal(object, int, int)


    def __init__(self, parent=None):
        super(DataAvailabilityPlotForm, self).__init__(parent=parent)
        self.dataavailui = Ui_data_avail_plot_Form()
        self.dataavailui.setupUi(self)

        # add the custom waveform plot widget to the stacked widget
        self.dataavailui.data_avail_plot_widget = DataAvailabilityPlotWidget()
        self.dataavailui.data_avail_stackedWidget.addWidget(self.dataavailui.data_avail_plot_widget)
        self.dataavailui.data_avail_stackedWidget.setCurrentWidget(self.dataavailui.data_avail_plot_widget)

        # self.dataavailui.data_avail_plot_widget.setContentsMargins(0, 0, 0, 0)
        self.dataavailui.data_avail_plot_widget.setSpacing(0)  # might not be necessary for you

        # variable to lock the details of an active plot until we are done with it
        self.active_lock = False


        self._state = {}




    def reset_view(self):
        self._state["data_avail_plots"][0].setXRange(
            self._state["data_avail_plots_min_time"],
            self._state["data_avail_plots_max_time"])

        for lri in self._state["time_roi"]:
            # set bounds of ROI
            lri.setBounds([self._state["data_avail_plots_min_time"]-self._state["frac_time"], self._state["data_avail_plots_max_time"]+self._state["frac_time"]])


    def data_avail_plot_mouseMoved(self, pos):
        # print("Mouse Moved in Data Availability Widget")

        # find the plot the mouse is over

        # if there is no lock in place, then find the active plot details
        if not self.active_lock:
            for _i, plot in enumerate(self._state["data_avail_plots"]):
                vb = plot.vb
                if plot.sceneBoundingRect().contains(pos):
                    self.active_plot = plot
                    self.active_plot_index = _i

                    # set tooltip with the data coordinates
                    self.dispMousePos(pos)


    def dispMousePos(self, pos):
        # Display current mouse coords if over the scatter plot area as a tooltip
        x_coord = UTCDateTime(self.active_plot.vb.mapSceneToView(pos).toPoint().x())
        time_tool = self.active_plot.setToolTip(x_coord.ctime())

    def plotDataAvailData(self, rec_int_array_dict, chan_loc_unique_set=None):
        if not chan_loc_unique_set is None:
            # create color map for the unique chan_locs
            enum_chan_loc_unique = list(enumerate(list(chan_loc_unique_set)))

            color_map_values = [(float(a)/len(chan_loc_unique_set), b) for a,b in enum_chan_loc_unique]

            # turn into dict
            color_map_dict = dict([(b, a) for a, b in color_map_values])

            # pick one to turn into an actual colormap
            # spectrumColormap = ColorMap(*zip(*Gradients["bipolar"]["ticks"]))

            data_colormap = cm.get_cmap('tab10')

        self.rec_int_array_dict = rec_int_array_dict

        def build_y_axis(chan_loc_list):

            enum_chan_loc = list(enumerate(chan_loc_list))

            # rearrange dict
            self.chan_loc_id_dict = dict([(b, a) for a, b in enum_chan_loc])

            self.y_axis_string = AxisItem(orientation='left')
            self.y_axis_string.setTicks([enum_chan_loc])

        def get_chan_loc_id(chan_loc):
            return (self.chan_loc_id_dict[chan_loc])


        print("plotting data", self.rec_int_array_dict)

        self._state["data_avail_plots"] = []
        self._state["time_roi"] = []

        starttimes = []
        endtimes = []

        # plot seperate plot for each net/sta with multiple lines corresponding to each channel

        for _i, net_sta in enumerate(self.rec_int_array_dict.keys()):
            build_y_axis(rec_int_array_dict[net_sta].keys())

            # set up plot
            plot = self.dataavailui.data_avail_plot_widget.addPlot(
                _i, 0,
                axisItems={'bottom': DateAxisItem(orientation='bottom',
                                                  utcOffset=0),
                           'left': self.y_axis_string})

            self._state["data_avail_plots"].append(plot)
            plot.hideAxis("bottom")

            plot.setLabel("left", text="<em>"+net_sta+"</em>")
            plot.show()

            plot.setMouseEnabled(x=True, y=False)

            # iterate through stations
            for chan_loc_key, rec_array in self.rec_int_array_dict[net_sta].items():

                print("Getting Color!!!!")
                print(chan_loc_key)
                print(color_map_dict[chan_loc_key])
                print(data_colormap)
                print(data_colormap(color_map_dict[chan_loc_key]))
                print(colors.to_hex(data_colormap(color_map_dict[chan_loc_key])))


                starttimes.append(rec_array[0][0])
                endtimes.append(rec_array[1][-1])

                # now go through each channel/loc and plot in differnet colour
                rec_midpoints = []
                chan_loc_ids = []
                diff_frm_mid_list = []

                # if not stn_key.split('.')[1] in self.select_sta:
                #     continue

                # iterate through gaps list
                for _j in range(rec_array.shape[1]):
                    diff_frm_mid = (rec_array[1, _j] - rec_array[0, _j]) / 2.0

                    diff_frm_mid_list.append(diff_frm_mid)

                    rec_midpoints.append(rec_array[0, _j] + diff_frm_mid)
                    chan_loc_ids.append(get_chan_loc_id(chan_loc_key))


                # Plot Error bar data recording intervals
                err = ErrorBarItem(x=np.array(rec_midpoints), y=np.array(chan_loc_ids), left=np.array(diff_frm_mid_list),
                                      right=np.array(diff_frm_mid_list), beam=0.06, pen=colors.to_hex(data_colormap(color_map_dict[chan_loc_key])))

                err.setZValue(10)

                plot.addItem(err)

            # set up the time slider roi
            lri = LinearRegionItem(
                values=[0, 1], movable=True)

            lri.sigRegionChangeFinished.connect(self.get_roi_data)
            # set the ROI creration progress to "initial" to start with
            self.roi_progress = "initial"

            self._state["time_roi"].append(lri)
            plot.addItem(lri)

            # set active plot widget and id
            self.active_plot = plot
            self.active_plot_index = _i


        self.dataavailui.data_avail_plot_widget.setNumberPlots(len(rec_int_array_dict.keys()))

        for plot in self._state["data_avail_plots"][1:]:
            plot.setXLink(self._state["data_avail_plots"][0])

        self._state["data_avail_plots"][-1].showAxis("bottom")

        self._state["data_avail_plots_min_time"] = min(starttimes)
        self._state["data_avail_plots_max_time"] = max(endtimes)

        # now calculate the span of min-max and calcualte buffer of 10%
        span = self._state["data_avail_plots_max_time"] - self._state["data_avail_plots_min_time"]
        self._state["frac_time"] = span * 0.1

        # now set axis limits
        for plot in self._state["data_avail_plots"]:
            plot.setLimits(xMin=self._state["data_avail_plots_min_time"]-self._state["frac_time"], xMax=self._state["data_avail_plots_max_time"]+self._state["frac_time"])


        self.dataavailui.data_avail_plot_widget.scene().sigMouseMoved.connect(self.data_avail_plot_mouseMoved)

        self.reset_view()

    def get_roi_data(self):
        print(self._state["time_roi"])
        print(self.active_plot_index)

        # look at the values for ROI for active plot
        current_region = self._state["time_roi"][self.active_plot_index].getRegion()

        # now change all other ROI's to match user edited roi
        self.set_time_slider_region(current_region[0], current_region[1], exclude_index=self.active_plot_index)





    def set_time_slider_region(self, start, end, exclude_index=None):

        # go ythrough the time sliders change ROI with start and end values
        # emit the signal that the ROI bounds have changed only if we are callaing this method when the user changes ROI in Data AVialaibiluty Plot
        if not exclude_index is None and not self.roi_progress in ["initial", "building"]:
            # User has edited ROI in Daata Avail plot
            # emit signal
            self.DataAvailRangeValuesChanged.emit(self, start, end)

        # set roi progress to building as we change all of the ROI values in every plot. to Avoid emitting roi change signal
        self.roi_progress = "building"
        # self.lri.setRegion((start, end))
        for _i, lri in enumerate(self._state["time_roi"]):
            if not _i == exclude_index:
                lri.setRegion((start, end))

        # set the roi progress to active, I.e. signals should be emitted
        self.roi_progress = "active"


class DataAvailabilityPlotWidget(GraphicsView):
    """Widget implementing a graphicsView with a single MultiPlotItem inside."""

    def __init__(self, parent=None):
        self.minPlotHeight = 80
        self.noplts = 0
        self.mPlotItem = MultiPlotItem.MultiPlotItem()
        GraphicsView.__init__(self, parent)
        self.enableMouse(False)
        self.setCentralItem(self.mPlotItem)
        ## Explicitly wrap methods from mPlotItem
        # for m in ['setData']:
        # setattr(self, m, getattr(self.mPlotItem, m))
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

    def __getattr__(self, attr):  ## implicitly wrap methods from plotItem
        if hasattr(self.mPlotItem, attr):
            m = getattr(self.mPlotItem, attr)
            if hasattr(m, '__call__'):
                return m
        raise AttributeError(attr)

    def setNumberPlots(self, noplts):
        # new method to set the number of plots in the multiplotitem
        # for some reason this is not being picked up in the MyMultiPlotWidget code shipped with pyqtgraph
        self.noplts = noplts
        self.resizeEvent(None)

    def setMinimumPlotHeight(self, min):
        """Set the minimum height for each sub-plot displayed.

        If the total height of all plots is greater than the height of the
        widget, then a scroll bar will appear to provide access to the entire
        set of plots.

        Added in version 0.9.9
        """
        self.minPlotHeight = min
        self.resizeEvent(None)

    def widgetGroupInterface(self):
        return (None, DataAvailabilityPlotWidget.saveState, DataAvailabilityPlotWidget.restoreState)

    def saveState(self):
        return {}
        # return self.plotItem.saveState()

    def restoreState(self, state):
        pass
        # return self.plotItem.restoreState(state)

    def close(self):
        self.mPlotItem.close()
        self.mPlotItem = None
        self.setParent(None)
        GraphicsView.close(self)

    def setRange(self, *args, **kwds):
        GraphicsView.setRange(self, *args, **kwds)
        if self.centralWidget is not None:
            r = self.range
            minHeight = self.noplts * self.minPlotHeight
            if r.height() < minHeight:
                r.setHeight(minHeight)
                r.setWidth(r.width() - self.verticalScrollBar().width())
            self.centralWidget.setGeometry(r)

    def resizeEvent(self, ev):
        if self.closed:
            return
        if self.autoPixelRange:
            self.range = QtCore.QRectF(0, 0, self.size().width(), self.size().height())
        DataAvailabilityPlotWidget.setRange(self, self.range, padding=0,
                                    disableAutoPixel=False)  ## we do this because some subclasses like to redefine setRange in an incompatible way.
        self.updateMatrix()


