from PyQt5 import QtWidgets
from PyQt5 import QtCore
from argo.utils.TableModels import HoldingsPandasModel
from argo.utils.TableModels import HoldingsCheckBoxDelegate


class HoldingsTableView(QtWidgets.QTableView):
    holdingsTableItemChecked = QtCore.pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        QtWidgets.QTableView.__init__(self, *args, **kwargs)


    def updateHoldingsTable(self, current_df):

        # Populate the holdings table using the custom Pandas table class
        self.holdings_model = HoldingsPandasModel(self)
        self.holdings_model.update(current_df)

        self.setItemDelegateForColumn(0, HoldingsCheckBoxDelegate(self))

        self.setModel(self.holdings_model)

