from PyQt5 import QtWidgets
from PyQt5 import QtCore

HOLDINGS_VIEW_ITEM_TYPES = {
    "SRC_DIR": 0,
    "VIRT_NETWORK": 1}

class HoldingsTreeWidget(QtWidgets.QTreeWidget):
    holdingsItemChecked = QtCore.pyqtSignal(object)

    def __init__(self, *args, **kwargs):
        QtWidgets.QTreeWidget.__init__(self, *args, **kwargs)

        # hide the header information
        self.header().hide()


    def updateHoldingsTree(self, holdings_dict, tree_selection_dict):


        # print(holdings_dict)

        tree_items = []

        for src_dir, virt_net_dict in holdings_dict.items():

            # add the src-dir and virt networks to tree
            src_dir_item = HoldingsTreeWidgetItem([src_dir],
                                                  type=HOLDINGS_VIEW_ITEM_TYPES["SRC_DIR"])

            for virt_net, holdings_df in virt_net_dict.items():
                # add the src_dir/virt network pair into selection dictionary
                tree_selection_dict[src_dir+"|"+virt_net] = "False"

                virt_net_item = HoldingsTreeWidgetItem([virt_net],
                                                  type=HOLDINGS_VIEW_ITEM_TYPES["VIRT_NETWORK"])

                virt_net_item.setFlags(virt_net_item.flags() | QtCore.Qt.ItemIsUserCheckable)
                virt_net_item.setCheckState(0, QtCore.Qt.Unchecked)

                src_dir_item.addChild(virt_net_item)

            tree_items.append(src_dir_item)

            self.insertTopLevelItems(0, tree_items)




def handle(self, item, column):
    self.treeWidget.blockSignals(True)
    if item.checkState(column) == QtCore.Qt.Checked:
        self.handleChecked(item, column)
    elif item.checkState(column) == QtCore.Qt.Unchecked:
        self.handleUnchecked(item, column)
    self.treeWidget.blockSignals(False)

class HoldingsTreeWidgetItem(QtWidgets.QTreeWidgetItem):
    def setData(self, column, role, value):
        state = self.checkState(column)
        QtWidgets.QTreeWidgetItem.setData(self, column, role, value)
        if (role == QtCore.Qt.CheckStateRole and
            state != self.checkState(column)):
            treewidget = self.treeWidget()
            if treewidget is not None:
                treewidget.holdingsItemChecked.emit(self)

