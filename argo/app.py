import sys

import qdarkstyle

from PyQt5 import QtWidgets
from argo.mainwindow import MainWindow

def run():
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    form = MainWindow()
    form.setMinimumSize(800, 600)
    form.show()
    form.raise_()

    sys.exit(app.exec_())


if __name__ == '__main__':
    run()