import sys
import os
import qdarkstyle
from PyQt5 import QtWidgets, QtCore

from functools import partial

from argo.widgets.TimeRangeSliderWidget import TimeRangeSliderForm
from argo.widgets.HoldingsTreeWidget import HoldingsTreeWidget
from argo.widgets.HoldingsTableView import HoldingsTableView
from argo.widgets.WaveformListWidget import WaveformListWidget
from argo.widgets.DataAvailabilityListWidget import DataAvailabilityListWidget

from argo.controls.SetButtonIcons import SetButtonIcons

from argo.ui.argo_window import Ui_MainWindow
from argo.ui import buildui

from argo.database.mock_awsS3 import holdingsDB
from argo.database.mock_mongoDB import metaDB

from argo.dialogs.TimingQCWindow import TimingQCWindow
from argo.dialogs.DataSelectDialog import DataSelectDialog

from argo.utils.DataReductionSelection import get_current_metadata_df
from argo.utils.DataReductionSelection import open_asdf_files
from argo.utils.DataReductionSelection import get_nscl_level_recording_intervals
from argo.utils.DataReductionSelection import get_detailed_recording_intervals
from argo.utils.DataReductionSelection import get_selected_metadata
from argo.utils.DataReductionSelection import extract_waveforms_frm_ASDF

from argo.widgets.WaveformPlotWidget import WaveformPlotForm


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent=parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # add in the holdings tree widget
        self.ui.holdings_treeWidget = HoldingsTreeWidget()
        self.ui.holdings_tree_stackedWidget.addWidget(self.ui.holdings_treeWidget)
        self.ui.holdings_tree_stackedWidget.setCurrentWidget(self.ui.holdings_treeWidget)

        # add in the holdings Table view
        self.ui.holdings_tableView = HoldingsTableView()
        self.ui.holdings_table_stackedWidget.addWidget(self.ui.holdings_tableView)
        self.ui.holdings_table_stackedWidget.setCurrentWidget(self.ui.holdings_tableView)

        # add in the custom waveform list widget
        self.ui.waveform_listWidget = WaveformListWidget()
        self.ui.waveform_stackedWidget.addWidget(self.ui.waveform_listWidget)
        self.ui.waveform_stackedWidget.setCurrentWidget(self.ui.waveform_listWidget)

        # add in data availabi;lity graph widget
        self.ui.data_availability_listWidget = DataAvailabilityListWidget()
        self.ui.data_availability_stackedWidget.addWidget(self.ui.data_availability_listWidget)
        self.ui.data_availability_stackedWidget.setCurrentWidget(self.ui.data_availability_listWidget)

        # connect when tab is changed
        self.ui.data_view_tabWidget.currentChanged.connect(self.data_view_tab_changed)

        # persistant dictionary for time range slider initialization values and recording intervals array
        self.time_range_slider_values_dict = {"roi_st": "", "roi_et": "","st": "", "et": "", "rec_array": ""}
        # add in the time range slider widget
        self.ui.time_range_slider_widget = TimeRangeSliderForm()
        self.ui.range_slider_stackedWidget.addWidget(self.ui.time_range_slider_widget)
        self.ui.range_slider_stackedWidget.setCurrentWidget(self.ui.time_range_slider_widget)

        self.connectTimeRangeSlider()

        # set all button icons
        SetButtonIcons.set_button_icons(self)

        # set the status for waveform plotting to False
        self.waveform_plot_status = "False"

        # BUild the map views
        # buildui.build_main_map_view(self)
        buildui.build_dataview_map_view(self)

        map_view_html = os.path.abspath(os.path.join(
            os.path.dirname(__file__), "../resources/quake_select.html"))
        map_view_js = os.path.abspath(os.path.join(
            os.path.dirname(__file__), "../resources/quake_select.js"))

        # print(map_view_html)

        self.ui.map_view_webEngine.load(QtCore.QUrl.fromLocalFile(map_view_html))
        self.ui.map_view_webEngine.loadFinished.connect(partial(self.onMapLoadFinished,
                                                                map=self.ui.map_view_webEngine,
                                                                js=map_view_js))

    def onMapLoadFinished(self, map, js):
        with open(js, 'r') as f:
            frame = map.page()


    @QtCore.pyqtSlot(object)
    def get_holdings_table_selected(self, obj):
        # print("Holdings Table Item Toggled")
        # print(obj)
        # print("______")
        # open up the file handlers for the selected ASDF files and corresponding JSON db files
        open_asdf_files(self)

        # now go through and create pandas meta table for selected files
        get_current_metadata_df(self)

        if not self.meta_current_df.empty:
            pass
            # get_recording_intervals_for_selected(self)
            # min_time = self.meta_current_df["StartDate"].min()
            # max_time = self.meta_current_df["EndDate"].max()

    @QtCore.pyqtSlot(object)
    def get_holdings_tree_selected(self, obj):
        tree_item_name = obj.parent().text(0) + "|" + obj.text(0)
        # now update the selection dictionary
        if self.holdings_tree_selection_dict[tree_item_name] == "True":
            self.holdings_tree_selection_dict[tree_item_name] = "False"
        elif self.holdings_tree_selection_dict[tree_item_name] == "False":
            self.holdings_tree_selection_dict[tree_item_name] = "True"

        # rebuild holdings table view
        buildui.build_holdings_table_view(self)

        # print(self.holdings_tree_selection_dict)

    @QtCore.pyqtSlot(int, int)
    def get_time_slider_range_values(self, st, et):


        # print("TimeRangeSlider Values Changed!")
        # print(st, et)
        self.time_slider_roi = (st, et)

        # change the roi in data vailability plot if it exists
        if hasattr(self, "data_avail_item_widget"):
            # set the region of lri
            buildui.data_availability_plot_range_update(self)


        # check if the waveform plot button is pressed
        if self.waveform_plot_status == "True":
            print("Time Slider Region Changed")
            print("Updating Waveform Plot!!!")
            # extract data for selection
            extract_waveforms_frm_ASDF(self)
            # now plot the extracted waveforms
            buildui.waveform_plot_update(self)

    @QtCore.pyqtSlot(object, int, int)
    def get_data_avail_range_values(self, active_data_avail_widget, st, et):
        # Signal when range values have been changed on Data AVailability plot by user
        print("============++++++++++++++==============")
        print("Dealing with DataAvail Range Changed")
        print(self.data_avail_roi_progress)
        print(active_data_avail_widget)
        print(st,et)
        #TODO modify time slider based on slider in data availability plot
        buildui.build_time_range_slider(self, roi_st=st, roi_et=et)
        # self.ui.time_range_slider_widget.set_time_slider_region(slider_dict["st"], slider_dict["et"])


        # if self.data_avail_roi_progress=="active":
        #     #Now update roi in time range slider
        #     self.time_slider_roi = (st, et)
        #     self.


        # # set the roi progress to building (i.e. we are busy settiing all other rois in other list widget to start and end values)
        # # this is to ignore the roi_changed signals
        # self.data_avail_roi_progress = "building"
        # # change all of teh ROIs in other list widget items
        # buildui.data_availability_plot_range_update(self, active_data_avail_widget=active_data_avail_widget)
        #
        # #set the roi progress to active (i.e. we should make use of the signals
        # self.data_avail_roi_progress = "active"



    @QtCore.pyqtSlot(object, object)
    def get_selected_nscl_data(self, unique_data_dict, data_sel_df):
        # print("Getting selected NSCL info!!!!")
        # print(unique_data_dict)
        self.selected_unique_NSCL_df_dict = unique_data_dict
        self.selected_NSCL_df = data_sel_df
        # get the metadata relevant to checked NSCL's
        get_selected_metadata(self)
        # get recording interval (stacked for all selected NSCL's)
        get_nscl_level_recording_intervals(self)
        # now get detailed recording intervals for each selected NSCL
        get_detailed_recording_intervals(self)
        # update the station availibility plot so that user knows that there is a new one
        #TODO: make sure user knows that there is a new station availability plot to build
        # now redraw the time slider
        buildui.build_time_range_slider(self, rec_array=self.metaDB.rec_int_array)


    @QtCore.pyqtSlot(bool)
    def get_waveform_plot_status(self, status):
        print("Status of Waveform plot changed!!!")
        # print(status)
        if status == True:
            self.waveform_plot_status = "True"
            print("Updating Waveform Plot!!!")
            # extract data for selection
            extract_waveforms_frm_ASDF(self)
            # now plot the extracted waveforms
            buildui.waveform_plot_update(self)
        else:
            self.waveform_plot_status = "False"

    @QtCore.pyqtSlot(int)
    def data_view_tab_changed(self, tab_index):
        print("Tab View Changed")
        print(tab_index)
        if tab_index == 0:
            # now build station availaibility plot
            buildui.data_availability_plot_update(self)
            # update the ROI range on every plot
            buildui.data_availability_plot_range_update(self)




    def connectHoldingsTableView(self):
        # print(self.ui.holdings_tableView.model)
        self.ui.holdings_tableView.holdingsTableItemChecked.connect(self.get_holdings_table_selected)

    def connectHoldingsTreeWidget(self):
        self.ui.holdings_treeWidget.holdingsItemChecked.connect(self.get_holdings_tree_selected)

    def connectDataSelectionDialog(self):
        self.DataSelectDialog.dataSelectionChanged.connect(self.get_selected_nscl_data)

    def connectTimeRangeSlider(self):
        slider = self.ui.time_range_slider_widget
        slider.RangeValuesChanged.connect(self.get_time_slider_range_values)
        slider.GoWaveformPlot.connect(self.get_waveform_plot_status)

    def connectDataAvailPlot(self, active_data_avail_widget):
        active_data_avail_widget.DataAvailRangeValuesChanged.connect(self.get_data_avail_range_values)




    def on_data_select_toolButton_released(self):
        # print("Opening Data Select Dialog")
        if not self.meta_current_df.empty:
            # get subset of meta df
            sub_df = self.meta_current_df[["Network", "Station", "Location", "Channel"]]

            # #check if the dialog is already open but might be hidden.
            # print(self.DataSelectDialog)
            # open up the dialog
            self.DataSelectDialog = DataSelectDialog(self, sub_df)
            self.connectDataSelectionDialog()

            # print(self.DataSelectDialog.selected_nscl)

    def on_cloud_link_pushButton_released(self):
        """
        Button to set-up cloud storage location
        """

        # # open dialog for selecting file holding DB
        # holdings_db_file = QtWidgets.QFileDialog.getOpenFileName(self, "Choose File Holdings DB", os.path.expanduser("~"), "JSON Files (*.json)")
        # if not holdings_db_file[0]:
        #     return
        #
        # # open dialog for selecting metadata DB
        # meta_db_file = QtWidgets.QFileDialog.getOpenFileName(self, "Choose Metadata DB", os.path.expanduser("~"), "JSON Files (*.json)")
        # if not meta_db_file[0]:
        #     return

        holdings_db_file = ["/Users/ashbycooper/Desktop/mock_AWS/file_holdings.json"]
        meta_db_file = ["/Users/ashbycooper/Desktop/mock_metaDB/metaDB.json"]

        self.holdingsDB = holdingsDB(json_file=holdings_db_file[0])
        self.metaDB = metaDB(json_file=meta_db_file[0])

        # turn the holdingsDB into dictionary of pandas dataframes
        self.holdingsDB.database_to_pandas()
        self.metaDB.database_to_pandas()

        # build metaDB numpy indexes
        self.metaDB.generateIndex()

        # set up dictionary for selected tree items
        self.holdings_tree_selection_dict = {}

        # rec_start_list = [UTCDateTime("2018-01-01T12:00:00").timestamp, UTCDateTime("2018-02-05T12:00:00").timestamp,
        #                   UTCDateTime("2018-02-12T12:00:00").timestamp]
        # rec_end_list = [UTCDateTime("2018-02-01T12:00:00").timestamp, UTCDateTime("2018-02-10T12:00:00").timestamp,
        #                 UTCDateTime("2018-03-12T12:00:00").timestamp]
        # rec_intervals_array = np.array([rec_start_list, rec_end_list])
        #
        # # set up dictionary for current recording intervals values on GUI wide time slider (use default values to start)
        # self.time_range_slider_values_dict = {"st": UTCDateTime("2018-01-05T12:00:00").timestamp, "et": UTCDateTime("2018-01-06T12:00:00").timestamp, "rec_array": rec_intervals_array}
        #
        # # build the time range slider based on initial values
        # buildui.build_time_range_slider(self)

        # now we can populate the file holdings view and main map view
        buildui.build_holdings_tree_view(self)
        # now build holdings table view based on selection in tree view
        buildui.build_holdings_table_view(self)




def main():
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())

    form = MainWindow()
    form.setMinimumSize(800, 600)
    form.show()
    form.raise_()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()